package edu.ntnu.idatt2001.group45;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import edu.ntnu.idatt2001.group45.data.Player;
import edu.ntnu.idatt2001.group45.filehandling.PathFileReader;
import edu.ntnu.idatt2001.group45.filehandling.PlayerReader;
import java.io.FileNotFoundException;
import org.junit.jupiter.api.Test;

public class PlayerReaderTest {
  

  @Test
  public void testReadPathFileValidFile() throws FileNotFoundException {
    PlayerReader playerReader = new PlayerReader();
    Player player = playerReader.readPlayerFile("Erica.Player");
    assertNotNull(player, "player should not be null when reading this file.");
    assertEquals("Erica", player.getName(), "The name of this character should be Erica'");

    assertEquals(70, player.getHealth(), "The health of this character should be'70'");

  }
}
