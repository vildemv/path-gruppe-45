package edu.ntnu.idatt2001.group45;

import edu.ntnu.idatt2001.group45.data.Story;
import edu.ntnu.idatt2001.group45.filehandling.PathFileReader;
import edu.ntnu.idatt2001.group45.filehandling.PathFileWriter;
import java.io.File;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PathFileWriterTest {

  PathFileReader pathFileReader = new PathFileReader();
  PathFileWriter pathFileWriter = new PathFileWriter();

  @Test
  @DisplayName("We want to check if the file that is written is valid, is the same as the object"
      + "and is readable. ")
  public void checkIfTheFileThatIsWrittenIsValid(){
    Story story = pathFileReader.readPathFile("src/main/resources/paths/TheMysteriousForest");
    File file = pathFileWriter.writeStory(story);
    Story story1 = pathFileReader.readPathFile(file.getAbsolutePath());

    assertTrue(file.isFile());
    assertEquals(story.getTitle(),story1.getTitle());
    assertEquals(story.getOpeningPassage().getTitle(),story1.getOpeningPassage().getTitle());
  }

}
