package edu.ntnu.idatt2001.group45;
import edu.ntnu.idatt2001.group45.data.Story;
import edu.ntnu.idatt2001.group45.filehandling.PathFileReader;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
public class PathFileReaderTest {
    @Test
    public void testReadPathFileValidFile() throws FileNotFoundException {
        String filePath = "src/test/resources/sampleStory.paths";
        PathFileReader fileReader = new PathFileReader();
        Story story = fileReader.readPathFile(filePath);
        assertNotNull(story, "Story should not be null for a valid file.");
        assertEquals("Pirate Story", story.getTitle(), "The story title should be 'Pirate Story'");
        assertEquals("Big ship", story.getOpeningPassage().getTitle(), "The opening passage should be 'Big ship'");
    }

    @Test
    public void testReadPathFileInvalidFile() {
        String filePath = "src/test/resources/nonExistentFile.paths";
        PathFileReader fileReader = new PathFileReader();
        Story story = fileReader.readPathFile(filePath);
        assertNull(story, "Story shuold be null cause it's an invalid file");
    }

}
