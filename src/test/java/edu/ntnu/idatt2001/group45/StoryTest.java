package edu.ntnu.idatt2001.group45;

import edu.ntnu.idatt2001.group45.data.Link;
import edu.ntnu.idatt2001.group45.data.Passage;
import edu.ntnu.idatt2001.group45.data.Story;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class StoryTest {
  Passage passage = new Passage("walk", "this way");
  Story story = new Story("this story", passage);

  @Nested
  class PositiveTest {
    @Test
    @DisplayName("Check if we can add passage to Story class")
    public void checkIfWeCanAddPassageToStoryClass() {

      boolean inStory = false;
      Passage passageToAdd = new Passage("the", "other way");
      story.addPassage(passageToAdd);

      if (story.getPassages().contains(passageToAdd)) {
        inStory = true;
      }
      assertTrue(inStory);
    }

    @Test
    public void removePassage(){
      Passage openingPassage = new Passage("opening passage", "opening content");

      Story test = new Story("story", openingPassage);

      Passage test1 = new Passage("test1", "test1 content");
      Passage test2 = new Passage("test2", "test2 content");

      test.addPassage(test1);
      test.addPassage(test2);

      Link linkToRemove = new Link("linkToRemove", "test1");

      test.removePassage(linkToRemove);

      assertEquals(1,test.getPassages().size());
    }

    @Test
    public void getBrokenLinksTest(){
      Passage openingPassage = new Passage("opening passage", "opening content");

      Story test = new Story("story", openingPassage);

      Passage test1 = new Passage("test1", "test1 content");
      Passage test2 = new Passage("test2", "test2 content");
      Link brokenLink = new Link("broken link", "test3");
      Link linkToTest1 = new Link("link to test1", "test1");
      test2.addLink(brokenLink);
      test2.addLink(linkToTest1);

      test.addPassage(test1);
      test.addPassage(test2);

      assertEquals(1,test.getBrokenLinks().size());
    }
  }

  @Nested
  public class NegativeTest{
    @Test
    public void removePassageWithLinks(){
      Passage openingPassage = new Passage("opening passage", "opening content");

      Story test = new Story("story", openingPassage);

      Passage test1 = new Passage("test1", "test1 content");
      Passage test2 = new Passage("test2", "test2 content");
      Link linkToTest1 = new Link("link to test1", "test1");

      test2.addLink(linkToTest1);

      test.addPassage(test1);
      test.addPassage(test2);

      Link linkToRemove = new Link("linkToRemove", "test1");

      try {
        test.removePassage(linkToRemove);
        fail("Initiation did not catch the exception");
      } catch (IllegalArgumentException ex){
        assertEquals("The passage is linked to other passages", ex.getMessage());
      }
    }
  }
}
