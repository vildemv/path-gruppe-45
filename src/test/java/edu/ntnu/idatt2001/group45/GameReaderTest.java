package edu.ntnu.idatt2001.group45;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.group45.data.Game;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import edu.ntnu.idatt2001.group45.data.goals.GoldGoal;
import edu.ntnu.idatt2001.group45.data.goals.HealthGoal;
import edu.ntnu.idatt2001.group45.data.goals.InventoryGoal;
import edu.ntnu.idatt2001.group45.data.goals.ScoreGoal;
import edu.ntnu.idatt2001.group45.filehandling.GameReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Nested;

public class GameReaderTest {
  GameReader gameReader = new GameReader();
  Game game = gameReader.readGameFromFile("src/main/resources/games/asdPirateStory.game");

  public GameReaderTest() throws IOException {
  }
    @Test
    public void checkIfGameIsNull() throws IOException {
      assertNotNull(game);
      assertNotNull(game.getPlayer());
      assertNotNull(game.getGoals());
      assertNotNull(game.getStory());
    }

    @Test
    public void readPlayerTest(){
      assertEquals("asd",game.getPlayer().getName());
      assertEquals(1414,game.getPlayer().getHealth());
      assertEquals(1414,game.getPlayer().getScore());
      assertEquals(0,game.getPlayer().getGold());
      assertTrue(game.getPlayer().getInventory().isEmpty());
    }

    @Test
    public void readStoryTest(){
      assertEquals("Pirate Story",game.getStory().getTitle());
      assertEquals(5,game.getStory().getPassages().size());
      assertEquals("Big ship",game.getStory().getOpeningPassage().getTitle());
    }

    @Test
    public void readGoalsTest(){
      for(Goal goal : game.getGoals()){
        if(goal instanceof GoldGoal){
          assertEquals(10,((GoldGoal) goal).getMinimumGold());
        }
        if(goal instanceof ScoreGoal){
          assertEquals(10,((ScoreGoal) goal).getMinimumScore());
        }
        if(goal instanceof HealthGoal){
          assertEquals(20,((HealthGoal) goal).getMinimumHealth());
        }
        if(goal instanceof InventoryGoal){
          assertEquals("[sword]", ((InventoryGoal) goal).getMandatoryItems().toString());
        }
      }
    }


  }

