package edu.ntnu.idatt2001.group45;

import edu.ntnu.idatt2001.group45.filehandling.GoalReader;
import edu.ntnu.idatt2001.group45.filehandling.GoalWriter;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import edu.ntnu.idatt2001.group45.data.goals.GoldGoal;
import edu.ntnu.idatt2001.group45.data.goals.HealthGoal;
import edu.ntnu.idatt2001.group45.data.goals.InventoryGoal;
import edu.ntnu.idatt2001.group45.data.goals.ScoreGoal;
import java.io.File;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class GoalWriterTest {
  GoalWriter goalWriter = new GoalWriter();

  GoalReader goalReader = new GoalReader();
  List<Goal> goals = new ArrayList<>();
  List<String> mandatoryItems = new ArrayList<>();

  GoldGoal goldGoal = new GoldGoal(100);
  ScoreGoal scoreGoal = new ScoreGoal(200);
  HealthGoal healthGoal = new HealthGoal(400);
  InventoryGoal inventoryGoal = new InventoryGoal(mandatoryItems);

  @Test
  public void checkIfGoalFileIsRight(){
    goals.add(goldGoal);
    goals.add(scoreGoal);
    goals.add(healthGoal);
    goals.add(inventoryGoal);
    mandatoryItems.add("sword of skill");
    mandatoryItems.add("bow of fatigue");


    File file = goalWriter.saveGoalsToFile(goals);
    assertTrue(file.isFile());
    for(Goal goal : goalReader.readGoalFromFile(file.getAbsolutePath())){
      if(goal instanceof GoldGoal){
        assertEquals(100,((GoldGoal) goal).getMinimumGold());
      }
      if(goal instanceof ScoreGoal){
        assertEquals(200,((ScoreGoal) goal).getMinimumScore());
      }
      if(goal instanceof HealthGoal){
        assertEquals(400,((HealthGoal) goal).getMinimumHealth());
      }
      if(goal instanceof InventoryGoal){
        assertEquals("[sword of skill, bow of fatigue]", ((InventoryGoal) goal).getMandatoryItems().toString());
      }
    }



  }
}
