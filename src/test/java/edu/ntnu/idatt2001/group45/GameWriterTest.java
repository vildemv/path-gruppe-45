package edu.ntnu.idatt2001.group45;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import edu.ntnu.idatt2001.group45.filehandling.GameReader;
import edu.ntnu.idatt2001.group45.filehandling.GameWriter;
import edu.ntnu.idatt2001.group45.data.Game;
import java.io.File;
import java.io.IOException;
import org.junit.jupiter.api.Test;

public class GameWriterTest {
  GameWriter gameWriter = new GameWriter();
  GameReader gameReader = new GameReader();

  public GameWriterTest() throws IOException {
  }

  @Test
  public void possibleToReadACreatedFile() throws IOException {
    Game game = gameReader.readGameFromFile("src/main/resources/games/asdPirateStory.game");
    File file = gameWriter.saveGameToFile(game);
    assertTrue(file.isFile());
    assertTrue(file.exists());
    assertTrue(file.canRead());
    assertEquals(gameReader.readGameFromFile(file.getAbsolutePath()).getStory().getTitle(),game.getStory().getTitle());
  }


}
