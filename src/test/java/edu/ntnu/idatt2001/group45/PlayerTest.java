package edu.ntnu.idatt2001.group45;

import edu.ntnu.idatt2001.group45.data.Player;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {
    Player player = new Player("John", 300, 50, 200);

    @Nested
    class PositiveTests {

        @Test
        @DisplayName("Check if player health is added correctly")
        public void addPlayerHealthToCheckIfItsCorrect() {
            player.addHealth(100);
            assertEquals(400, player.getHealth());
        }

        @Test
        @DisplayName("Check if player health is removed correctly")
        public void removePlayerHeatlhToCheckIfItsCorrect() {
            player.removeHealth(100);
            assertEquals(200, player.getHealth());
        }


        @Test
        @DisplayName("Check if player score is added correctly")
        public void addPlayerScoreToCheckIfItsCorrect() {
            player.addScore(100);
            assertEquals(150, player.getScore());
        }

        @Test
        @DisplayName("Check if player score is removed correctly")
        public void removePlayerScoreToCheckIfItsCorrect() {
            player.removeScore(100);
            assertEquals(50, player.getScore());
        }

        @Test
        @DisplayName("Check if player gold is added correctly")
        public void addPlayerGoldToCheckIfItsCorrect() {
            player.addGold(150);
            assertEquals(350, player.getGold());
        }

        @Test
        @DisplayName("Check if player gold is removed correctly")
        public void removePlayerGOldTOCheckIfItsCorrect() {
            player.removeGold(-100);
            assertEquals(100, player.getGold());
        }

        @Test
        @DisplayName("Check if item is added to inventory")
        public void checkIfInventoryIsAddedCorrectly() {
            boolean inInventory = false;
            player.addToInventory("Sword");
            if (player.getInventory().contains("Sword")) {
                inInventory = true;
            }
            assertTrue(inInventory);
        }

        @Test
        @DisplayName("trying to use the builder pattern to create a player")
        public void builderTest(){
            Player player = new Player.Builder("John").gold(100).health(10).inventory("").build();
            assertEquals(player.getGold(),100);
            assertEquals(player.getHealth(),10);
            assertEquals(player.getName(), "John");
            assertEquals(player.getScore(),0);
        }

    }

    @Nested
    class NegativeTests {

        @Test
        @DisplayName("Test to see if it's possible for player to add negative gold")
        public void checkWhatHappensIfPlayerGoldNegative() {
            try {
                player.addGold(-1234);
            } catch (IllegalArgumentException ex){
                assertEquals(ex.getMessage(), "this is not possible");
            }
        }

        @Test
        @DisplayName("Test to see if it's possible for player add a negative score")
        public void checkWhatHappensIfScoreNegative() {
            try {
                player.addScore(-1234);
            } catch (IllegalArgumentException ex){
                assertEquals(ex.getMessage(), "this is not possible");
            }
        }

        @Test
        @DisplayName("Test to see if it's possible for player to add negative health")
        public void checkWhatHappensIfAddNegativeHealth(){
            try {
                player.addHealth(-1234);
            } catch (IllegalArgumentException ex){
                assertEquals(ex.getMessage(), "this is not possible");
            }
        }



        @Test
        @DisplayName("Testing initiation of negative health in player")
        public void InitiationOfNegativeHealthInPlayer() {
            try {
                Player p = new Player("Robert", -312, 15, 15);
                fail("initiation didn't throw IllegalArgumentException");
            } catch (IllegalArgumentException ex) {
                assertEquals(ex.getMessage(), "The health can not be below zero");
            }
        }

        @Test
        @DisplayName("Testing initiation of negative gold in player")
        public void InitiationOfNegativeGoldInPlayer() {
            try {
                Player p = new Player("Robert", 100, 15, -15);
                fail("initiation didn't throw IllegalArgumentException");
            } catch (IllegalArgumentException ex) {
                assertEquals(ex.getMessage(), "The gold can not be below zero");
            }
        }

        @Test
        @DisplayName("Testing initiation of negative score in player")
        public void InitiationOfNegativeScoreInPlayer() {
            try {
                Player p = new Player("Robert", 100, -15, 15);
                fail("initiation didn't throw IllegalArgumentException");
            } catch (IllegalArgumentException ex) {
                assertEquals(ex.getMessage(), "The score can not be below zero");
            }
        }
    }
}
