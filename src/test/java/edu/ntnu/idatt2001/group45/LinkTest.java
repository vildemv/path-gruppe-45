package edu.ntnu.idatt2001.group45;
import edu.ntnu.idatt2001.group45.data.Link;
import edu.ntnu.idatt2001.group45.data.actions.Action;
import edu.ntnu.idatt2001.group45.data.actions.ScoreAction;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LinkTest {
  Link link = new Link("hello","world");

  @Nested
  class PositiveTest{
    @Test
    @DisplayName("Check if we can add action to link class")
    public void checkIfWeCanAddActionToLinkClass(){
      boolean inActions = false;
      Action action = new ScoreAction(10);
      link.addAction(action);
      if(link.getActions().contains(action)){
        inActions = true;
      }
      assertTrue(inActions);
    }
  }
}
