package edu.ntnu.idatt2001.group45;
 import edu.ntnu.idatt2001.group45.data.Link;
 import edu.ntnu.idatt2001.group45.data.Passage;
 import org.junit.jupiter.api.DisplayName;
 import org.junit.jupiter.api.Nested;
 import org.junit.jupiter.api.Test;

 import static org.junit.jupiter.api.Assertions.assertTrue;

public class PassageTest {
    Link link = new Link("Walk", "star");
    Passage passage = new Passage("the runaway", "we are running");
    @Nested
    class PositiveTests{
        @Test
        @DisplayName("Check if we can add link to passage")
        public void checkIfWeCanAddLinkToPassage(){
            assertTrue(passage.addLink(link));
        }

    }
}
