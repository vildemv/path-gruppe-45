package edu.ntnu.idatt2001.group45;

import edu.ntnu.idatt2001.group45.data.Player;
import edu.ntnu.idatt2001.group45.filehandling.PlayerReader;
import edu.ntnu.idatt2001.group45.filehandling.PlayerWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class PlayerWriterTest {

    PlayerWriter playerWriter = new PlayerWriter();

    PlayerReader playerReader = new PlayerReader();
    Player player = new Player("John", 100, 150, 200);
    @Test
    public void playerWriterTest() throws FileNotFoundException {
      player.addToInventory("Sword of Magic");
      player.addToInventory("boots");

      File file = playerWriter.savePlayerToFile(player);

      Player player2 = playerReader.readPlayerFile(file.getAbsolutePath());

      assertEquals(player.getHealth(),player2.getHealth());
      assertEquals(player.getName(),player2.getName());
      assertEquals(player.getGold(),player2.getGold());
      assertEquals(player.getInventory().get(0).equals("Sword of Magic"),player2.getInventory().get(0).equals("Sword of Magic"));
    }


}
