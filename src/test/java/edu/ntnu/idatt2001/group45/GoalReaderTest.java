package edu.ntnu.idatt2001.group45;

import edu.ntnu.idatt2001.group45.data.goals.GoldGoal;
import edu.ntnu.idatt2001.group45.data.goals.HealthGoal;
import edu.ntnu.idatt2001.group45.data.goals.InventoryGoal;
import edu.ntnu.idatt2001.group45.data.goals.ScoreGoal;
import edu.ntnu.idatt2001.group45.filehandling.GoalReader;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GoalReaderTest {

    GoalReader goalReader = new GoalReader();
    List<Goal> goals = goalReader.readGoalFromFile("src/main/resources/goals/theEnchantedPrincess.goals");
    @Test
    public void testReadGoal(){
    for (Goal goal : goals){
      if(goal instanceof HealthGoal){
        Assertions.assertEquals(15,((HealthGoal) goal).getMinimumHealth());
      }
      if(goal instanceof ScoreGoal){
        Assertions.assertEquals(15,((ScoreGoal) goal).getMinimumScore());
      }
      if(goal instanceof GoldGoal){
        Assertions.assertEquals(15,((GoldGoal) goal).getMinimumGold());
      }
      if(goal instanceof InventoryGoal){
        Assertions.assertEquals("[item]",((InventoryGoal) goal).getMandatoryItems().toString());
      }
    }

  }
}
