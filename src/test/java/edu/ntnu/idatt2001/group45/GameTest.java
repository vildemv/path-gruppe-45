package edu.ntnu.idatt2001.group45;

import edu.ntnu.idatt2001.group45.data.Game;
import edu.ntnu.idatt2001.group45.data.Passage;
import edu.ntnu.idatt2001.group45.data.Player;
import edu.ntnu.idatt2001.group45.data.Story;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameTest {
    @Nested
    class PositiveTests{
        Player p = new Player("John",150,50,50);
        List<Goal> goalList = new ArrayList<>();
        Passage dogPassage = new Passage("The arrival", "Run away from this dog");
        Story s = new Story("The blind dog", dogPassage);
        Game g = new Game(p,s,goalList);


        @Test
        @DisplayName("Check if the opening passage is returned when we use begin")
        public void CheckIfTheOpeningPassageIsReturnedWhenWeUseBegin(){
            assertEquals(dogPassage, g.begin());

        }

    }
}
