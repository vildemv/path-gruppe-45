package edu.ntnu.idatt2001.group45.scenes;

import java.util.HashMap;
import java.util.Map;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This is a sceneSwitcher class. This class is used to switch scenes, instead of creating a new
 * scene everytime the user goes to next scene. This class uses a Map to get overview and getting
 * the Scene.
 */
public class SceneSwitcher {

  private final Map<String, Scene> scenes;
  private final Stage primaryStage;

  /**
   * the constructor for the SceneSwitcher.
   *
   * @param primaryStage the stage that the SceneSwitcher will be used on.
   */
  public SceneSwitcher(Stage primaryStage) {
    this.primaryStage = primaryStage;
    scenes = new HashMap<>();
  }

  /**
   * a method for adding a Scene to the SceneSwitcher.
   *
   * @param key   a key, that is usually the name of the scene
   * @param scene the scene we want to put inside the map
   */
  public void addScene(String key, Scene scene) {
    scenes.put(key, scene);
  }

  /**
   * a method to remove a scene.
   *
   * @param scene the key of the scene inside the map.
   */
  public void removeScene(String scene) {
    scenes.remove(scene);
  }

  /**
   * a method for switching the scene in the stage. This also set the title of the stage to the key
   * that is written in.
   *
   * @param key the key of the scene inside the scenes-map.
   */
  public void switchScene(String key) {
    Scene scene = scenes.get(key);
    primaryStage.setTitle("Paths: " + key);
    primaryStage.setScene(scene);
  }

}