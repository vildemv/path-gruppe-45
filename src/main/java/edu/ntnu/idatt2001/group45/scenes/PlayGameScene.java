package edu.ntnu.idatt2001.group45.scenes;


import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.controllers.PlayGameController;
import edu.ntnu.idatt2001.group45.data.Game;
import edu.ntnu.idatt2001.group45.data.Link;
import edu.ntnu.idatt2001.group45.data.Passage;
import edu.ntnu.idatt2001.group45.data.Player;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import edu.ntnu.idatt2001.group45.data.goals.GoldGoal;
import edu.ntnu.idatt2001.group45.data.goals.HealthGoal;
import edu.ntnu.idatt2001.group45.data.goals.InventoryGoal;
import edu.ntnu.idatt2001.group45.data.goals.ScoreGoal;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * this is the PlayGameScene. This is the scene/layout for playing the game. This scene contains a
 * TextField which is displaying the passage, and hyperlinks which is the links for the current
 * passages. Here the player will have alternatives to either play, exit or restart the game.
 */
public class PlayGameScene extends Scene {

  BorderPane root;

  private Passage currentPassage;

  private PlayGameController controller;

  private final Label label = new Label("Story: ");

  private final DataModel dataModel = DataModel.getInstance();
  private final VBox linkBox = new VBox();

  private final TextArea textArea = new TextArea();

  private final Label nameLabel;
  private final Label goldLabel;
  private final Label healthLabel;
  private final Label scoreLabel;
  private final Label inventoryLabel;
  private final Label goldGoalLabel;
  private final Label healthGoalLabel;
  private final Label scoreGoalLabel;
  private final Label inventoryGoalLabel;
  private Game game;

  /**
   * The constructor for the playGame Scene. It initates the layout for the game, and displays the
   * game if the datamodel checks if there are any game.
   *
   * @param primaryStage  the primary stage
   * @param sceneSwitcher the sceneswitcher
   * @param width         the width
   * @param height        the height
   */
  public PlayGameScene(Stage primaryStage, SceneSwitcher sceneSwitcher, double width,
      double height) {
    super(new BorderPane(), width, height);
    root = (BorderPane) getRoot();
    controller = new PlayGameController(primaryStage, sceneSwitcher);
    initiateGameWhenEverythingIsSet();
    currentPassage = null;
    showGameInfo();
    linkBox();

    nameLabel = new Label("Name:");
    goldLabel = new Label("Gold:");
    healthLabel = new Label("Health:");
    scoreLabel = new Label("Score:");
    inventoryLabel = new Label("Inventory: ");

    goldGoalLabel = new Label("Gold: ");
    healthGoalLabel = new Label("Health: ");
    scoreGoalLabel = new Label("Gold: ");
    inventoryGoalLabel = new Label("Inventory: ");

    root.setCenter(centerLayout());
    root.setBottom(bottomLayout());
    root.setRight(rightSideLayout());
  }

  /**
   * Creates a VBox. This VBox contains the textarea that displays the passage content and an info
   * box which displays the info for the game.
   *
   * @return returns the vbox, which we can use to initiate in the constructor.
   */
  public VBox centerLayout() {
    VBox centerContent = new VBox();
    centerContent.setAlignment(Pos.CENTER);
    centerContent.setSpacing(20);
    centerContent.setPadding(new Insets(10, 0, 0, 20));

    textArea.setEditable(false);
    textArea.setPrefSize(500, 400);

    HBox infoBox = new HBox();
    infoBox.getChildren().addAll(label, nameLabel);

    centerContent.getChildren().addAll(infoBox, textArea);
    return centerContent;
  }

  /**
   * The right side of the layout is a vBox, inside the VBox there is a HBox that contains a text
   * and a linkBox which is a container that contains all the hyperlinks that is created when a
   * passage is displayed.
   *
   * @return returns the vBox, which is used to in initation in the constructor.
   */
  public VBox rightSideLayout() {
    VBox rightBox = new VBox();
    rightBox.setPrefHeight(root.getHeight());
    rightBox.setPadding(new Insets(100, 0, 0, 10));
    rightBox.setPrefWidth(250);
    rightBox.setSpacing(10);
    rightBox.setAlignment(Pos.TOP_LEFT);

    HBox textBox = new HBox();
    Label text = new Label("Choose a path below to continue: ");
    text.setUnderline(true);
    textBox.getChildren().add(text);

    linkBox.setAlignment(Pos.TOP_LEFT);

    rightBox.getChildren().addAll(textBox, linkBox);
    return rightBox;
  }

  /**
   * the bottom layout of the game. This lays out all the labels for the player info and player
   * goals for the game, in addition to the button options.
   *
   * @return returns a HBox
   */
  public HBox bottomLayout() {
    HBox bottomContent = new HBox();
    bottomContent.setAlignment(Pos.TOP_LEFT);
    bottomContent.setPrefHeight(200);
    bottomContent.setPadding(new Insets(20, 0, 0, 20));
    bottomContent.setSpacing(10);

    VBox playerStatsBox = new VBox();
    playerStatsBox.setPrefWidth(100);
    playerStatsBox.setAlignment(Pos.TOP_LEFT);
    nameLabel.textProperty().bind(dataModel.playerObjectProperty().asString());
    playerStatsBox.getChildren().addAll(healthLabel, goldLabel, scoreLabel, inventoryLabel);

    VBox playerGoalBox = new VBox();
    playerGoalBox.setPrefWidth(100);
    playerGoalBox.setAlignment(Pos.TOP_LEFT);

    nameLabel.textProperty().bind(dataModel.getGoalsObject().asString());
    playerGoalBox.getChildren()
        .addAll(healthGoalLabel, goldGoalLabel, scoreGoalLabel, inventoryGoalLabel);

    HBox buttonBox = new HBox();
    buttonBox.setPadding(new Insets(0, 0, 0, 65));
    buttonBox.setAlignment(Pos.TOP_LEFT);
    buttonBox.setSpacing(10);

    Button exitButton = new Button("Exit");
    Button restartButton = new Button("Restart");
    Button helpButton = new Button("Help");

    exitButton.setPrefWidth(75);
    restartButton.setPrefWidth(75);
    helpButton.setPrefWidth(75);

    exitButton.setOnAction(event -> controller.exitAndSaveGame());
    restartButton.setOnAction(event -> controller.restartGame());
    helpButton.setOnAction(event -> controller.helpPage());

    buttonBox.getChildren().addAll(restartButton, exitButton, helpButton);

    bottomContent.getChildren().addAll(playerStatsBox, playerGoalBox, buttonBox);
    return bottomContent;
  }

  /**
   * a way to initiate or display game, when the user has chosen a game or loads a game. In this
   * method there are two different outcomes.
   * <p>
   * If the user chooses to play a new game, then it will initialize game and display character info
   * and show starting passsage.
   * <p>
   * If the user chooses to load game, the game will continue from where the user stopped playing.
   */
  private void initiateGameWhenEverythingIsSet() {
    if (dataModel.getPlayer() != null && dataModel.getStory() != null
        && dataModel.getGoals() != null) {
      loadGameData();
    }
    dataModel.playerObjectProperty().addListener((observable, oldValue, newValue) -> {
      if (newValue != null && dataModel.getStory() != null && dataModel.getGoals() != null) {
        initializeGame();
        updateDisplayStats();
        showStartingPassage();
      }
    });

    dataModel.storyProperty().addListener((observable, oldValue, newValue) -> {
      if (newValue != null && dataModel.getPlayer() != null && dataModel.getGoals() != null) {
        initializeGame();
        updateDisplayStats();
        showStartingPassage();
      }
    });

    dataModel.getGoalsObject().addListener((observable, oldValue, newValue) -> {
      if (newValue != null && dataModel.getPlayer() != null && dataModel.getStory() != null) {
        initializeGame();
        updateDisplayStats();
        showStartingPassage();
      }
    });
  }

  /**
   * a method to show Game info if the player has chosen a game. In this method there are listeners
   * that observes if the player has chosen a game, labels will show accordingly to the game.
   */
  public void showGameInfo() {
    dataModel.getGameObjectProperty().addListener((observableValue, game, t1) -> {
      if (t1 != null) {
        label.setText("Story: " + t1.getStory().getTitle() + " |  Broken links: " + t1.getStory()
            .brokenLinksToString());
        nameLabel.setText(" | Player: " + t1.getPlayer().getName());
      }
    });
  }

  /**
   * a method to display the starting passsage in the textArea,and setting the currentPassage to the
   * opening passage.
   */
  private void showStartingPassage() {
    currentPassage = game.getStory().getOpeningPassage();
    textArea.setText(currentPassage.getTitle() + "\n" + "\n" + currentPassage.getContent());
    createHyperlinks();
  }

  /**
   * a method for creating hyperlinks accordingly with the current passage the user is in.
   */
  private void createHyperlinks() {
    linkBox.getChildren().clear();

    for (Link link : currentPassage.getLinks()) {
      Hyperlink hyperlink = new Hyperlink("> " + link.getText());
      hyperlink.setPrefWidth(250);

      boolean isBrokenLink = game.getStory().brokenLink(link);

      switch (isBrokenLink ? "broken" : "not_broken") {
        case "not_broken" -> hyperlink.setOnAction(event -> handleLink(link));
        case "broken" -> hyperlink.setOnAction(event -> handleBrokenLink());
      }
      linkBox.getChildren().add(hyperlink);
    }
  }

  /**
   * a method to handle a link in the hyperlink. If the player clicks on the link they will get to
   * the next passage, if they are not dead. If going through the next passage kills the player the
   * game will end, or if the passage has no links the game will end etc.
   *
   * @param link the link the player is going through
   */
  private void handleLink(Link link) {
    try {
      currentPassage = game.go(link);
      dataModel.setPassage(currentPassage);
      updateDisplayStats();

      if (game.getPlayer().getHealth() != 0) {
        if (!currentPassage.hasLinks()) {
          initiateGameOver("Congratulations!\n \n" + currentPassage.getContent());
        } else {
          passThroughPassage();
        }
      } else {
        if (!currentPassage.hasLinks()) {
          initiateGameOver("GAME OVER! \n \n" + currentPassage.getContent());
        } else {
          initiateGameOver("GAME OVER! \n \n You made too many wrong choices.");
        }
      }
    } catch (IllegalArgumentException exception) {
      System.out.println(exception.getMessage());
    }
  }

  /**
   * if there are a broken link, then the game won't continue and the user has to start over again.
   */
  private void handleBrokenLink() {
    controller.GameOver("This link is broken!");
  }

  /**
   * A method for changing the textArea and creating hyperlinks based on the current passage the
   * player is in.
   */
  private void passThroughPassage() {
    if (currentPassage != null) {
      textArea.setText(currentPassage.getTitle() + "\n" + "\n" + currentPassage.getContent());
      createHyperlinks();
    }
  }

  /**
   * initiation of the link box.
   */
  public void linkBox() {
    linkBox.setAlignment(Pos.CENTER);
    linkBox.setPrefHeight(282);
    linkBox.setPrefWidth(100);
  }

  /**
   * An initiation of the game variable. It creates a game from the dataModel info.
   */
  private void initializeGame() {
    game = new Game(dataModel.getPlayer(), dataModel.getStory(), dataModel.getGoals());
  }

  /**
   * a method for updating the display stats of the player, including a tooltip over effect when the
   * user hover their mouse on the inventory- and inventory-goal label.
   */
  public void updateDisplayStats() {

    nameLabel.textProperty().unbind();
    healthLabel.textProperty().unbind();
    scoreLabel.textProperty().unbind();
    goldLabel.textProperty().unbind();
    inventoryLabel.textProperty().unbind();

    goldGoalLabel.textProperty().unbind();
    healthGoalLabel.textProperty().unbind();
    scoreLabel.textProperty().unbind();
    inventoryGoalLabel.textProperty().unbind();

    Player player = game.getPlayer();

    healthLabel.setText("Health: " + player.getHealth());
    scoreLabel.setText("Score: " + player.getScore());
    goldLabel.setText("Gold: " + player.getGold());
    inventoryLabel.setText("Inventory " + player.getInventory().toString());
    Tooltip tooltip = new Tooltip(player.getInventory().toString());
    Tooltip.install(inventoryLabel, tooltip);
    inventoryLabel.setOnMouseEntered(
        event -> tooltip.show(inventoryLabel, event.getScreenX(), event.getScreenY() + 20));
    inventoryLabel.setOnMouseExited(event -> tooltip.hide());
    for (Goal goal : dataModel.getGoals()) {
      if (goal instanceof GoldGoal) {
        goldGoalLabel.setText("Gold goal: " + player.getGold());
      }
      if (goal instanceof ScoreGoal) {
        scoreGoalLabel.setText("Score: " + ((ScoreGoal) goal).getMinimumScore());
      }
      if (goal instanceof HealthGoal) {
        healthGoalLabel.setText("Health: " + ((HealthGoal) goal).getMinimumHealth());
      }
      if (goal instanceof InventoryGoal) {
        inventoryGoalLabel.setText("Inventory: " + ((InventoryGoal) goal).getMandatoryItems());
        Tooltip tooltip1 = new Tooltip(((InventoryGoal) goal).getMandatoryItems().toString());
        Tooltip.install(inventoryGoalLabel, tooltip1);
        inventoryGoalLabel.setOnMouseEntered(
            event -> tooltip1.show(inventoryGoalLabel, event.getScreenX(),
                event.getScreenY() + 20));
        inventoryGoalLabel.setOnMouseExited(event -> tooltip1.hide());
      }
    }
  }

  /**
   * a method for initiating ending the game for the user.
   *
   * @param message the message in the game over.
   */
  private void initiateGameOver(String message) {
    controller.GameOver(message);
    if (dataModel.getGoals() != null && dataModel.getStory() != null
        && dataModel.getPlayer() != null) {
      updateDisplayStats();
    }

  }

  /**
   * A method for loading game data. When the player loads game, the listener checks if the
   * DataModel has a value or is null, if the DataModel has a value then it will load in these
   * values and display it in the game.
   */
  private void loadGameData() {
    updateDisplayStats();
    showCurrentPassage();
  }

  /**
   * a method to show the currentPassage and creating hyperlinks accordingly to the passage the user
   * is in.
   */
  private void showCurrentPassage() {
    currentPassage = dataModel.getPassage(); // Retrieve the current passage from the DataModel
    textArea.setText(currentPassage.getTitle() + "\n" + currentPassage.getContent());
    createHyperlinks();
  }
}

