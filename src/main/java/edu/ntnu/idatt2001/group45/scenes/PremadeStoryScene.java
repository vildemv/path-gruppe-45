package edu.ntnu.idatt2001.group45.scenes;

import edu.ntnu.idatt2001.group45.controllers.PremadeStoryController;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * This is the PreMadeStoryScene. This scene contains three buttons, which allows the user to choose
 * story that already are prewritten.
 */
public class PremadeStoryScene extends Scene {

  private final BorderPane root;
  private final PremadeStoryController premadeStoryController;

  /**
   * The constructor for the PremadeStoryScene. In the constructor, it's created the top, center and
   * the bottom layout for the root (which is a borderPane).
   *
   * @param primaryStage  the stage the scene is in
   * @param sceneSwitcher the sceneswitcher object
   * @param width         the width of the scene
   * @param height        the height of the scene
   */
  public PremadeStoryScene(Stage primaryStage, SceneSwitcher sceneSwitcher, double width,
      double height) {
    super(new BorderPane(), width, height);
    root = (BorderPane) getRoot();
    premadeStoryController = new PremadeStoryController(primaryStage, sceneSwitcher);

    createTop();
    createCenter();
    createBottom();
  }

  /**
   * the top layout of the HBox. Inside the Box it contains a text
   */
  public void createTop() {
    HBox topContent = new HBox();
    topContent.setAlignment(Pos.CENTER);
    topContent.setPrefHeight(root.getHeight() / 3.0);

    Text describingText = new Text("Choose a premade story ");

    topContent.getChildren().addAll(describingText);
    root.setTop(topContent);
  }

  /**
   * The center layout of the scene. This layout contains three buttons, these buttons are the
   * options the user can choose to play a pre-made story.
   */
  public void createCenter() {
    HBox centerContent = new HBox();
    centerContent.setAlignment(Pos.CENTER);
    centerContent.setPrefHeight(root.getHeight() / 3.0);

    FlowPane storyBox = new FlowPane();

    storyBox.setAlignment(Pos.CENTER);
    storyBox.setHgap(10);
    storyBox.setVgap(10);

    Button story1 = new Button("Pirate Story");
    Button story2 = new Button("The Enchanted Princess");
    Button story3 = new Button("The mysterious forest");

    story1.setOnAction(event -> premadeStoryController.readPirateStory());
    story2.setOnAction(event -> premadeStoryController.readTheEnchantedPrincess());
    story3.setOnAction(even -> premadeStoryController.readTheMysteriousForest());

    storyBox.getChildren().addAll(story1, story2, story3);
    centerContent.getChildren().addAll(storyBox);
    root.setCenter(centerContent);


  }

  /**
   * a method for creating the bottom layout fo the game, which contains a hyperlink that lets the
   * player go back to the mainMenu.
   */
  private void createBottom() {
    HBox bottomContent = new HBox();
    bottomContent.setAlignment(Pos.CENTER);
    bottomContent.setPrefHeight(root.getHeight() / 3.0);

    Hyperlink mainMenu = new Hyperlink(" << go back >> ");
    mainMenu.setOnAction(event -> premadeStoryController.onHyperlinkClick1());

    bottomContent.getChildren().addAll(mainMenu);
    root.setBottom(bottomContent);
  }

}
