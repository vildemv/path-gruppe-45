package edu.ntnu.idatt2001.group45.scenes;

import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.controllers.MainMenuController;
import java.util.Objects;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * this is the class for the Main Menu scene.
 */
public class MainMenuScene extends Scene {

  private final BorderPane root;
  private final MainMenuController controller;

  /**
   * the constructor for the main menu scene.
   *
   * @param primaryStage  the primary stage of this scene
   * @param sceneSwitcher the sceneswitcher of this scne
   * @param width         the width of this scene
   * @param height        the height of this scene.
   */
  public MainMenuScene(Stage primaryStage, SceneSwitcher sceneSwitcher, double width,
      double height) {
    super(new BorderPane(), width, height);

    root = (BorderPane) getRoot();
    controller = new MainMenuController(primaryStage, sceneSwitcher);

    createCenterLayout();
    createTopLayout();
  }

  /**
   * creates the center layout for the main menu scene. Here are the buttons created and given
   * functionality through the controller.
   */
  private void createCenterLayout() {
    VBox center = new VBox();
    center.setAlignment(Pos.TOP_CENTER);
    center.setPadding(new Insets(34.5, 0, 0, 0));
    center.setSpacing(10);

    Button newStoryButton = new Button("New Story");
    Button loadStoryButton = new Button("Load Story");
    Button exitButton = new Button("Exit");

    newStoryButton.setPrefWidth(100);
    loadStoryButton.setPrefWidth(100);
    exitButton.setPrefWidth(100);

    newStoryButton.setOnAction(event -> controller.onButtonClick1());
    loadStoryButton.setOnAction(event -> controller.onButtonClick2());
    exitButton.setOnAction(event -> controller.onButtonClick3());

    center.getChildren().addAll(newStoryButton, loadStoryButton, exitButton);

    root.setCenter(center);

    loadStoryButton.disableProperty().bind(isDataModelNull());

  }

  /**
   * a method to get a booleanBinding that checks if any of the DatModel values are null.
   *
   * @return returns a booleanBinding.
   */
  private BooleanBinding isDataModelNull() {
    return Bindings.createBooleanBinding(
        () -> DataModel.getInstance().getPlayer() == null
            ||
            DataModel.getInstance().getStory() == null
            ||
            DataModel.getInstance().getGoals() == null,
        DataModel.getInstance().playerObjectProperty(),
        DataModel.getInstance().storyProperty(),
        DataModel.getInstance().getGoalsObject()
    );
  }


  /**
   * initiates and creates the top layout of the main menu scene Creates the logo for the main menu
   * and set it the top of the screen.
   */
  private void createTopLayout() {
    HBox top = new HBox();
    top.setAlignment(Pos.CENTER);
    top.setPadding(new Insets(50, 0, 0, 0));

    Image logoImage = new Image(
        Objects.requireNonNull(getClass().getResourceAsStream("/images/logo.png")));
    ImageView logoImageView = new ImageView(logoImage);

    logoImageView.setFitHeight(200);
    logoImageView.setFitWidth(450);

    top.getChildren().addAll(logoImageView);
    root.setTop(top);

  }


}

