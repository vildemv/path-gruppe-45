package edu.ntnu.idatt2001.group45.scenes;

import edu.ntnu.idatt2001.group45.controllers.NewStoryController;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * the newStory scene is a scene that initiates a layout for the user to either choose between
 * playing a pre-made story or uploading their own story.
 */
public class NewStoryScene extends Scene {

  private final BorderPane root;
  private final NewStoryController newStoryController;

  /**
   * the constructor for the newStoryScene. This initiates and creates the whole layout through
   * private methods
   *
   * @param primaryStage  the stage of this scene.
   * @param sceneSwitcher the sceneswitcher component used to switch scenes
   * @param width         the width of the scene
   * @param height        the height of the scene.
   */
  public NewStoryScene(Stage primaryStage, SceneSwitcher sceneSwitcher, double width,
      double height) {
    super(new BorderPane(), width, height);

    root = (BorderPane) getRoot();
    newStoryController = new NewStoryController(primaryStage, sceneSwitcher);

    createCenter();
    createTop();
    createBottom();
  }

  /**
   * Creates the center of the newStory scene. This layout contains a container which holds buttons.
   * These buttons let the user choose between uploading their own story or having a pre-made story
   */
  public void createCenter() {
    HBox centerContent = new HBox();
    centerContent.setAlignment(Pos.CENTER);
    centerContent.setPrefHeight(root.getHeight() / 3.0);
    centerContent.setSpacing(10);

    Button premadeStory = new Button("Premade story");
    Button uploadStory = new Button("Upload story");

    premadeStory.setOnAction(action -> newStoryController.onButtonClick1());
    uploadStory.setOnAction(actionEvent -> newStoryController.onButtonClick2());

    centerContent.getChildren().addAll(premadeStory, uploadStory);
    root.setCenter(centerContent);
  }

  /**
   * creates the top layout of the NewStory scne. This layout only contains a text.
   */
  public void createTop() {
    HBox topContent = new HBox();
    topContent.setAlignment(Pos.CENTER);
    topContent.setPrefHeight(root.getHeight() / 3.0);

    Text describingText = new Text("Choose a premade story or upload your own story ");

    topContent.getChildren().addAll(describingText);
    root.setTop(topContent);
  }

  /**
   * creates the bottom layout of the NewStory scene. This layout only contains a return hyperlink
   * for the root.
   */
  public void createBottom() {
    HBox bottomContent = new HBox();
    bottomContent.setAlignment(Pos.CENTER);
    bottomContent.setPrefHeight(root.getHeight() / 3.0);

    Hyperlink mainMenu = new Hyperlink(" << go back >>");
    mainMenu.setOnAction(event -> newStoryController.onHyperlinkClick1());

    bottomContent.getChildren().addAll(mainMenu);
    root.setBottom(bottomContent);
  }

}