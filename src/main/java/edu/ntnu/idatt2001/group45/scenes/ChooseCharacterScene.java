package edu.ntnu.idatt2001.group45.scenes;

import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.controllers.ChooseCharacterController;
import edu.ntnu.idatt2001.group45.data.Player;
import edu.ntnu.idatt2001.group45.filehandling.PlayerReader;
import java.io.File;
import java.io.FileNotFoundException;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * This class is the scene for choosing the character in the program. This scene displays available
 * characters and is interactive for the user to click on the displays.
 */
public class ChooseCharacterScene extends Scene {

  private final BorderPane root;
  private final ChooseCharacterController controller;
  private final DataModel dataModel = DataModel.getInstance();
  private final FlowPane playerListPane = new FlowPane();
  private final Label storyTitleLabel = new Label();
  private final Label storyPathLabel = new Label();

  /**
   * The constructor for the ChooseCharacterScene.
   *
   * @param primaryStage  the primary stage of this scene
   * @param sceneSwitcher the sceneswitcher component used to switch between different scenes
   * @param width         the width of this scene
   * @param height        the height of this scene
   * @throws FileNotFoundException if the File is not found
   */
  public ChooseCharacterScene(Stage primaryStage, SceneSwitcher sceneSwitcher, double width,
      double height)
      throws FileNotFoundException {
    super(new BorderPane(), width, height);

    root = (BorderPane) getRoot();
    controller = new ChooseCharacterController(primaryStage, sceneSwitcher);

    createTopContent();
    createCenterContent();
    createBottomContent();

    updateStoryTitleLabel();
    updateStoryPathLabel();
    updateCharacterDisplay();
    displayAvailablePlayers();

  }

  /**
   * This is the initiation of the content in the top of the borderpane. This layout contains
   * different type of text, like info given to the player or a display of which story the player
   * has chosen.
   */
  public void createTopContent() {
    VBox topContent = new VBox();
    topContent.setPrefHeight(root.getPrefHeight() / 3);
    topContent.setAlignment(Pos.TOP_CENTER);
    topContent.setSpacing(65);

    HBox describingTextBox = new HBox();
    HBox storyTitleBox = new HBox();

    storyTitleBox.setPadding(new Insets(10, 0, 0, 10));

    describingTextBox.setAlignment(Pos.CENTER);
    storyTitleBox.setAlignment(Pos.TOP_LEFT);

    Text describingText = new Text("Choose a premade player or create your own player");

    describingTextBox.getChildren().addAll(describingText);
    storyTitleBox.getChildren().addAll(storyTitleLabel);

    topContent.getChildren().addAll(storyTitleBox, describingTextBox);
    root.setTop(topContent);
  }

  /**
   * this is a method for initiating the center content of this scene. The method initiates the
   * preferred height and width for the flowpane and set it in the center.
   */
  public void createCenterContent() {
    playerListPane.setPadding(new Insets(125, 0, 0, 0));
    playerListPane.setPrefHeight(200);
    playerListPane.setHgap(15);
    playerListPane.setVgap(15);
    playerListPane.setAlignment(Pos.TOP_CENTER);
    root.setCenter(playerListPane);
  }

  /**
   * a method for creating the bottom content of the scene. This creates a container for the add new
   * character button.
   */
  public void createBottomContent() {
    VBox bottomContent = new VBox();
    bottomContent.setPadding(new Insets(50, 0, 0, 0));
    bottomContent.setAlignment(Pos.TOP_CENTER);
    bottomContent.setPrefHeight(500);
    bottomContent.setPrefWidth(root.getPrefWidth());
    bottomContent.setSpacing(50);

    VBox buttonAndHyperlinkBox = new VBox();
    buttonAndHyperlinkBox.setSpacing(75);
    buttonAndHyperlinkBox.setAlignment(Pos.TOP_CENTER);

    Button button = new Button("Create your own player");
    button.setOnAction(actionEvent -> controller.showCreatePlayerScene());

    Hyperlink hyperlink = new Hyperlink("<< go back >>");
    hyperlink.setOnAction(event -> controller.onHyperLinkClick());

    buttonAndHyperlinkBox.getChildren().addAll(button, hyperlink);

    bottomContent.getChildren().addAll(buttonAndHyperlinkBox, storyPathLabel);

    root.setBottom(bottomContent);
  }

  /**
   * a method for displaying all the players based on the player-folder. if there are no players it
   * will display a label which says "no players found"
   *
   * @throws FileNotFoundException throws FileNotFoundException if it cannot read the given file.
   */
  private void displayAvailablePlayers() throws FileNotFoundException {
    playerListPane.getChildren().clear(); // Clear existing player labels
    File folder = new File(
        "src/main/resources/players"); // Replace with the path to your "players" folder
    File[] playerFiles = folder.listFiles();
    PlayerReader playerReader = new PlayerReader();
    if (playerFiles != null) {
      for (File playerFile : playerFiles) {
        if (playerFile.isFile()) {
          try {
            Player player = playerReader.readPlayerFile(playerFile.getAbsolutePath());
            Image iconImage = new Image("/images/playericon.png");
            ImageView iconImageView = new ImageView(iconImage);
            iconImageView.setFitWidth(80);
            iconImageView.setPreserveRatio(true);

            Label playerLabel = new Label(player.getName(), iconImageView);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("health: ").append(player.getHealth()).append("\ngold: ")
                .append(player.getGold()).append("\nscore: ").append(player.getScore())
                .append("\ninventory:").append(player.getInventory().toString());
            playerLabel.setGraphic(iconImageView);
            playerLabel.setContentDisplay(ContentDisplay.TOP);
            setUpToolTip(playerLabel, stringBuilder);
            playerLabel.setOnMouseClicked(event -> {
              controller.handlePlayerLabelClick(player);

            });
            playerListPane.getChildren().add(playerLabel);
          } catch (IllegalArgumentException e) {
            System.out.println("Invalid player file under resources: " + playerFile.getName());
          }
        }
      }
    } else {
      Label noPlayersLabel = new Label("No players found.");
      playerListPane.getChildren().add(noPlayersLabel);
    }
  }

  /**
   * A method for setting up tooltip for the player display, when the user hover their mouse to the
   * icon.
   *
   * @param playerLabel   the label/icon of the player
   * @param stringBuilder the content inside the tooltip
   */
  static void setUpToolTip(Label playerLabel,
      StringBuilder stringBuilder) {
    Tooltip tooltip = new Tooltip(stringBuilder.toString());
    Tooltip.install(playerLabel, tooltip);

    playerLabel.setOnMouseEntered(
        event -> tooltip.show(playerLabel, event.getScreenX(), event.getScreenY() + 20));
    playerLabel.setOnMouseExited(event -> tooltip.hide());
  }

  /**
   * Method to update the displayed story title.
   */
  private void updateStoryTitleLabel() {
    updateLabel(dataModel, storyTitleLabel);
  }

  /**
   * a method for updating the story label and the broken link label when a user has chosen a
   * story.
   *
   * @param dataModel the datamodel of the system
   * @param storyTitleLabel the label to be displayed
   */
  static void updateLabel(DataModel dataModel, Label storyTitleLabel) {
    dataModel.storyProperty().addListener((observableValue, story, newValue) -> {
      if (newValue != null && dataModel.getFile() == null) {
        storyTitleLabel.setText(
            "Story: " + newValue.getTitle() + " | Broken links: " + newValue.brokenLinksToString());
      }
    });
  }

  /**
   * Method to display the story file path when a file is upload and chosen by the user.
   */
  private void updateStoryPathLabel() {
    dataModel.getFileProperty().addListener((observableValue, file, t1) -> {
      if (t1 != null) {
        storyPathLabel.setText(t1.getAbsolutePath());
      } else {
        storyPathLabel.setText("");
      }
    });
  }

  /**
   * Method to update the display of characters whenever a new character is added and set to the
   * datamodel.
   */
  private void updateCharacterDisplay() {
    dataModel.playerObjectProperty().addListener(new ChangeListener<Player>() {
      @Override
      public void changed(ObservableValue<? extends Player> observableValue, Player player,
          Player t1) {
        try {
          displayAvailablePlayers();
        } catch (FileNotFoundException e) {
          throw new RuntimeException(e);
        }
      }
    });
  }
}
