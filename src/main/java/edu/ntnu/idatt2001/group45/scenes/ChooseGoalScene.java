package edu.ntnu.idatt2001.group45.scenes;

import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.controllers.ChooseGoalController;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import edu.ntnu.idatt2001.group45.filehandling.GoalReader;
import edu.ntnu.idatt2001.group45.filehandling.GoalWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The class for the ChooseGoalScene. This class initiates and creates the layout for choosing the
 * goal
 */
public class ChooseGoalScene extends Scene {

  private final BorderPane root;
  private final DataModel dataModel = DataModel.getInstance();

  private final FlowPane goalListPane = new FlowPane();

  private final Label storyTitleLabel = new Label();

  private final Label playerLabel = new Label();
  private final Label storyPathLabel = new Label();
  private final ChooseGoalController controller;

  /**
   * the constructor for chooseGoalScene, this constructor runs all the private methods that
   * initiates the layout.
   *
   * @param primaryStage  the stage it is in
   * @param sceneSwitcher the sceneswitcher inside this class
   * @param width         the width of this scene
   * @param height        the height of this scene
   * @throws FileNotFoundException if a specific goal file is not found in the goals folder.
   */
  public ChooseGoalScene(Stage primaryStage, SceneSwitcher sceneSwitcher, double width,
      double height)
      throws FileNotFoundException {
    super(new BorderPane(), width, height);
    root = (BorderPane) getRoot();
    controller = new ChooseGoalController(primaryStage, sceneSwitcher);

    createCenterLayout();
    createTopLayout();
    createBottomLayout();

    displayFromGoalSet();

    updateDisplay();
    updatePlayerLabel();
    updateStoryTitleLabel();
    updateStoryPathLabel();
  }

  /**
   * a listener that updates the display whenever a user creates a new gaol object, and displays it
   * on the screen.
   */
  public void updateDisplay() {
    dataModel.getGoalsObject().addListener(
        (ObservableValue<? extends List<Goal>> observable, List<Goal> oldGoals, List<Goal> newGoals) -> {
          try {
            displayFromGoalSet();
          } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
          }
        });
  }

  /**
   * Method to dislpay chosen story at the top of the screen.
   */
  private void updateStoryTitleLabel() {
    ChooseCharacterScene.updateLabel(dataModel, storyTitleLabel);
  }

  /**
   * Method to display chosen player at the top of the screen.
   */
  private void updatePlayerLabel() {
    dataModel.playerObjectProperty().addListener((observableValue, player, newValue) -> {
      if (newValue != null) {
        playerLabel.setText(" | Player: " + newValue.getName());
      }
    });
  }

  /**
   * Method to display the story file path if a file has been chosen.
   */
  private void updateStoryPathLabel() {
    dataModel.getFileProperty().addListener((observableValue, file, newFile) -> {
      if (newFile != null) {
        storyPathLabel.setText(newFile.getAbsolutePath());
      } else {
        storyPathLabel.setText("");
      }
    });
  }


  /**
   * a method for creating the center layout of this program. It initiates the center layout for the
   * flowpane and sets it center in the root.
   */
  private void createCenterLayout() {
    goalListPane.setPadding(new Insets(55, 0, 0, 0));
    goalListPane.setPrefHeight(200);
    goalListPane.setHgap(30);
    goalListPane.setVgap(30);
    goalListPane.setAlignment(Pos.TOP_CENTER);
    root.setCenter(goalListPane);
  }

  /**
   * a method for creating the top layout for the ChooseGoals scene.
   */
  private void createTopLayout() {
    VBox topContent = new VBox();
    topContent.setPrefHeight(root.getHeight() / 3);
    topContent.setAlignment(Pos.TOP_CENTER);
    topContent.setSpacing(42.5);

    HBox describingTextBox = new HBox();
    HBox infoBox = new HBox();

    infoBox.setSpacing(0);
    infoBox.setPadding(new Insets(10, 0, 0, 10));

    infoBox.setAlignment(Pos.TOP_LEFT);
    describingTextBox.setAlignment(Pos.CENTER);

    Text describingText = new Text("Choose premade goals or create your own goals");

    describingTextBox.getChildren().addAll(describingText);
    infoBox.getChildren().addAll(storyTitleLabel, playerLabel);

    topContent.getChildren().addAll(infoBox, describingTextBox);
    root.setTop(topContent);
  }

  /**
   * a method for creating the bottom layout of the ChooseGoal Scene.
   */
  private void createBottomLayout() {
    VBox bottomContent = new VBox();
    bottomContent.setPadding(new Insets(56.5, 0, 0, 0));
    bottomContent.setAlignment(Pos.TOP_CENTER);
    bottomContent.setPrefHeight(500);
    bottomContent.setPrefWidth(root.getPrefWidth());
    bottomContent.setSpacing(50);

    VBox buttonAndHyperlinkBox = new VBox();
    buttonAndHyperlinkBox.setSpacing(75);
    buttonAndHyperlinkBox.setAlignment(Pos.TOP_CENTER);

    Button button = new Button("Create your own goals");
    button.setOnAction(ActionEvent -> controller.createGoals());

    Hyperlink hyperlink = new Hyperlink("<< go back >>");
    hyperlink.setOnAction(event -> controller.onHyperLinkClick());

    buttonAndHyperlinkBox.getChildren().addAll(button, hyperlink);

    bottomContent.getChildren().addAll(buttonAndHyperlinkBox, storyPathLabel);

    root.setBottom(bottomContent);
  }

  /**
   * a method for displaying existing goal sets in the scene.
   *
   * @throws FileNotFoundException if the file is not found
   */
  private void displayFromGoalSet() throws FileNotFoundException {
    goalListPane.getChildren().clear(); // Clear existing player labels
    File folder = new File(
        "src/main/resources/goals"); // Replace with the path to your "players" folder
    File[] goalFiles = folder.listFiles();
    GoalReader goalReader = new GoalReader();
    if (goalFiles != null) {
      for (File goalFile : goalFiles) {
        if (goalFile.isFile()) {
          try {
            List<Goal> goals = goalReader.readGoalFromFile(goalFile.getAbsolutePath());
            Image iconImage = new Image("/images/flag_icon.png");
            ImageView iconImageView = new ImageView(iconImage);
            iconImageView.setFitWidth(40);
            iconImageView.setPreserveRatio(true);
            StringBuilder sb = new StringBuilder();
            GoalWriter writer = new GoalWriter();
            for (Goal goal : goals) {
              sb.append(writer.convertGoalToString(goal)).append("\n");
            }

            Label goalLabel = new Label(goalFile.getName().replace(".goals", ""));
            goalLabel.setGraphic(iconImageView);
            goalLabel.setContentDisplay(ContentDisplay.TOP);
            createToolTip(sb, goalLabel);
            goalLabel.setOnMouseClicked(event -> {
              dataModel.setGoals(goals);
              controller.setGoalToDataModel(goals);
            });
            goalListPane.getChildren().add(goalLabel);
          } catch (IllegalArgumentException e) {
            System.out.println("Invalid goal file under resources: " + goalFile.getName());
          }

        }
      }
    } else {
      Label noPlayersLabel = new Label("No Goals found.");
      goalListPane.getChildren().add(noPlayersLabel);
    }
  }

  /**
   * a static methods that borrows the method from the ChooseCharacterScene. Used to create tooltip
   * so the user can see the display of content when they hover their mouse in the node.
   *
   * @param sb        the content inside the display
   * @param goalLabel the label that gets interacted with
   */
  protected static void createToolTip(StringBuilder sb, Label goalLabel) {
    ChooseCharacterScene.setUpToolTip(goalLabel, sb);
  }

}
