package edu.ntnu.idatt2001.group45.scenes;

import edu.ntnu.idatt2001.group45.controllers.MainMenuController;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class LoadStoryScene extends Scene {
  public LoadStoryScene(Stage primaryStage, SceneSwitcher sceneSwitcher, double width, double height) {
    super(new BorderPane(), width, height);
  }
}

