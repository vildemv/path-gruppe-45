package edu.ntnu.idatt2001.group45.controllers;

import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.data.Story;
import edu.ntnu.idatt2001.group45.filehandling.PathFileReader;
import edu.ntnu.idatt2001.group45.scenes.SceneSwitcher;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

/**
 * The controller for the PremadeStoryScene. This class handles buttons and action by the user.
 */
public class PremadeStoryController {

  private final Stage primaryStage;
  private final SceneSwitcher sceneSwitcher;

  /**
   * the constructor for the premadeStoryController.
   *
   * @param primaryStage  the primary stage
   * @param sceneSwitcher the sceneswitcher component
   */
  public PremadeStoryController(Stage primaryStage, SceneSwitcher sceneSwitcher) {
    this.primaryStage = primaryStage;
    this.sceneSwitcher = sceneSwitcher;
  }

  /**
   * a method for returning to the main menu.
   */
  public void onHyperlinkClick1() {
    sceneSwitcher.switchScene("New Story");
  }

  /**
   * a method for the player to read the first pre-made story. This method tries to locate the
   * pre-made story inside this project, and uses a pathFileReader to get and set the Story to the
   * dataModel. When it has set the value for the dataModel, it switches to the next scene
   */
  public void readPirateStory() {
    try {
      String filePath = "src/main/resources/paths/PirateStory.path";
      PathFileReader fileReader = new PathFileReader();
      Story story = fileReader.readPathFile(filePath);
      DataModel.getInstance().setStory(story);
      sceneSwitcher.switchScene("Choose Character");
    } catch (IllegalArgumentException | NullPointerException e) {
      showErrorAlert();
    }
  }

  /**
   * a method for the player to read the second pre-made story. This method tries to locate the
   * pre-made story inside this project, and uses a pathFileReader to get and set the Story to the
   * dataModel. When it has set the value for the dataModel, it switches to the next scene
   */
  public void readTheEnchantedPrincess() {
    try {
      String filePath = "src/main/resources/paths/TheEnchantedPrincess.path";
      PathFileReader fileReader = new PathFileReader();
      Story story = fileReader.readPathFile(filePath);
      DataModel.getInstance().setStory(story);
      sceneSwitcher.switchScene("Choose Character");
    } catch (IllegalArgumentException | NullPointerException e) {
      showErrorAlert();
    }
  }

  /**
   * a method for the player to read the third pre-made story. This method tries to locate the
   * pre-made story inside this project, and uses a pathFileReader to get and set the Story to the
   * dataModel. When it has set the value for the dataModel, it switches to the next scene
   */
  public void readTheMysteriousForest() {
    try {
      String filePath = "src/main/resources/paths/TheMysteriousForest.path";
      PathFileReader fileReader = new PathFileReader();
      Story story = fileReader.readPathFile(filePath);
      DataModel.getInstance().setStory(story);
      sceneSwitcher.switchScene("Choose Character");
    } catch (IllegalArgumentException | NullPointerException e) {
      showErrorAlert();
    }
  }

  /**
   * a method to get error if there are errors in the pre-made stories.
   */
  private void showErrorAlert() {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText(null);
    alert.setContentText("Invalid format in the .path file!");
    alert.showAndWait();
  }

}
