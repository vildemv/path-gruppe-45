package edu.ntnu.idatt2001.group45.controllers;

import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.data.Player;
import edu.ntnu.idatt2001.group45.filehandling.PlayerWriter;
import edu.ntnu.idatt2001.group45.scenes.SceneSwitcher;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * the controller class for the ChooseCharacterScene.
 */
public class ChooseCharacterController {

  Stage primaryStage;
  SceneSwitcher sceneSwitcher;

  /**
   * the constructor for the ChooseCharacterController
   *
   * @param primaryStage  the primary stage
   * @param sceneSwitcher the sceneSwitcher
   */
  public ChooseCharacterController(Stage primaryStage, SceneSwitcher sceneSwitcher) {
    this.primaryStage = primaryStage;
    this.sceneSwitcher = sceneSwitcher;
  }

  /**
   * a method that handles when the user clicks on the player icons. When the user clicks on the
   * icons, the dataModel set the player in the dataModel, and switch scenes.
   *
   * @param player
   */
  public void handlePlayerLabelClick(Player player) {
    try {
      DataModel.getInstance().setPlayer(player);
      System.out.println("Selected player: " + DataModel.getInstance().getPlayer().getName());
      sceneSwitcher.switchScene("Choose Goals");
    } catch (IllegalArgumentException exception) {
      showErrorAlert(exception.getMessage());
    }
  }

  /**
   * a method to return to the main menu.
   */
  public void onHyperLinkClick() {
    if (DataModel.getInstance().getFile() != null) {
      DataModel.getInstance().setFile(null);
    }
    sceneSwitcher.switchScene("New Story");

  }

  /**
   * a method that creates a popup-window where the user can create their own player.
   */
  public void showCreatePlayerScene() {
    Stage createPlayerStage = new Stage();
    createPlayerStage.setTitle("Create Player");
    createPlayerStage.setResizable(false);
    createPlayerStage.initOwner(primaryStage);

    // Create the UI controls
    Label nameLabel = new Label("Name:");
    TextField nameTextField = new TextField();

    Label scoreLabel = new Label("Score:");
    TextField scoreTextField = new TextField();
    Label healthLabel = new Label("Health:");
    TextField healthTextField = new TextField();

    Label goldLabel = new Label("Gold:");
    TextField goldTextField = new TextField();
    Label inventoryLabel = new Label("Inventory");
    TextField inventoryTextField = new TextField();

    FlowPane inventoryItemsContainer = new FlowPane();

    inventoryItemsContainer.setHgap(5);
    inventoryItemsContainer.setVgap(5);
    inventoryItemsContainer.setPadding(new Insets(5));

    List<String> inventoryValue = new ArrayList<>();

    Button addItemButton = new Button("+");
    addItemButton.setOnAction(event -> {
      String item = inventoryTextField.getText().trim();
      if (!item.isEmpty()) {
        Label itemLabel = new Label(item);
        inventoryItemsContainer.getChildren().add(itemLabel);
        inventoryValue.add(item);
        inventoryTextField.clear();

        Button removeButton = new Button("-");
        removeButton.setOnAction(removeEvent -> {
          inventoryItemsContainer.getChildren().removeAll(itemLabel, removeButton);
          inventoryValue.remove(item);
        });

        inventoryItemsContainer.getChildren().addAll(removeButton);

        // Clear the text field for the next item
        inventoryTextField.clear();
      }
    });
    Button saveButton = new Button("Continue");
    saveButton.setOnAction(event -> {
      String playerName = nameTextField.getText();
      int score = 0;
      int health = 0;
      int gold = 0;
      if (playerName.isBlank()) {
        showErrorAlert("Please give the character a name.");
        return;
      }
      // Validate and parse the score value
      try {
        score = Integer.parseInt(scoreTextField.getText());
      } catch (IllegalArgumentException e) {
        // Handle invalid score input
        showErrorAlert("Invalid score value. Please enter a positive numeric value.");
        return;
      }

      // Validate and parse the health value
      try {
        health = Integer.parseInt(healthTextField.getText());
      } catch (IllegalArgumentException e) {
        // Handle invalid health input
        showErrorAlert("Invalid health value. Please enter a positive numeric value.");
        return;
      }

      // Validate and parse the gold value
      try {
        gold = Integer.parseInt(goldTextField.getText());
      } catch (IllegalArgumentException e) {
        // Handle invalid gold input
        showErrorAlert("Invalid gold value. Please enter a positive numeric value.");
        return;
      }
      try {
        Player player = new Player.Builder(playerName)
            .score(score)
            .health(health)
            .gold(gold)
            .build();
        for (String item : inventoryValue) {
          player.addToInventory(item);
        }
        PlayerWriter playerWriter = new PlayerWriter();
        playerWriter.savePlayerToFile(player);
        DataModel.getInstance().setPlayer(player);
        enablePrimaryStage();
        createPlayerStage.close();
        sceneSwitcher.switchScene("Choose Goals");
      } catch (IllegalArgumentException e) {
        showErrorAlert(e.getMessage());
      }


    });

    // Create the layout
    VBox layout = new VBox();
    layout.setPrefSize(400, 500);

    layout.setAlignment(Pos.CENTER);
    layout.setPadding(new Insets(10));
    layout.setSpacing(10);

    layout.getChildren().addAll(nameLabel, nameTextField, scoreLabel, scoreTextField,
        healthLabel, healthTextField, goldLabel, goldTextField, inventoryLabel, inventoryTextField,
        addItemButton, inventoryItemsContainer, saveButton);
    Scene createPlayerScene = new Scene(layout);
    createPlayerStage.setScene(createPlayerScene);
    primaryStage.getScene().getRoot().setDisable(true);
    createPlayerStage.setOnHidden(event -> enablePrimaryStage());
    createPlayerStage.showAndWait();
  }

  /**
   * a method to show error alerts whenever the user does something invalid
   *
   * @param errorMessage the message to be displayed.
   */
  private void showErrorAlert(String errorMessage) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText(null);
    alert.setContentText(errorMessage);
    alert.showAndWait();
  }

  /**
   * a method to enable the primary stage when the pop-up stage is closed.
   */
  private void enablePrimaryStage() {
    // Enable the primary stage when the popup stage is closed
    primaryStage.getScene().getRoot().setDisable(false);
  }

}