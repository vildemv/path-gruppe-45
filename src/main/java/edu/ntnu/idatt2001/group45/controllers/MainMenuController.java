package edu.ntnu.idatt2001.group45.controllers;

import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.scenes.SceneSwitcher;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

/**
 * The controller for the Main menu class, it gives behavior for all the buttons in this class.
 */
public class MainMenuController {

  Stage primaryStage;
  SceneSwitcher sceneSwitcher;

  /**
   * the constructor for the MainMenuController.
   *
   * @param primaryStage  the primary stage
   * @param sceneSwitcher the sceneSwitcher
   */
  public MainMenuController(Stage primaryStage, SceneSwitcher sceneSwitcher) {
    this.primaryStage = primaryStage;
    this.sceneSwitcher = sceneSwitcher;
  }

  /**
   * a method for switching to the NewGameScene.
   */
  public void onButtonClick1() {
    if (DataModel.getInstance().getPlayer() != null && DataModel.getInstance().getStory() != null
        && DataModel.getInstance().getGoals() != null) {
      boolean confirmation = showConfirmationAlert(
          "You already are in a game, do you want to override your progress?");
      if (confirmation) {
        DataModel.getInstance().resetDataModel();
        sceneSwitcher.switchScene("New Story");
      }
    } else {
      sceneSwitcher.switchScene("New Story");
    }
  }

  /**
   * a method for switching to the PlayGameScene.
   */
  public void onButtonClick2() {
    sceneSwitcher.switchScene("Play Game");
  }

  /**
   * a method for exiting the program.
   */
  public void onButtonClick3() {
    primaryStage.close();
  }

  /**
   * a method for creating an alert that lets the user confirm whether the user wants or not want to
   * make a choice.
   *
   * @param confirmationMessage the message to be displayed in the alert
   * @return true if the user clicks yes, returns false if the user clicks no.
   */
  private boolean showConfirmationAlert(String confirmationMessage) {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setTitle("Confirmation");
    alert.setHeaderText(null);
    alert.setContentText(confirmationMessage);

    ButtonType buttonYes = new ButtonType("Yes");
    ButtonType buttonNo = new ButtonType("No");
    alert.getButtonTypes().setAll(buttonYes, buttonNo);

    // Show the confirmation alert and wait for the user's response
    Optional<ButtonType> result = alert.showAndWait();

    // Return true if the user clicked "Yes", false otherwise
    return result.orElse(ButtonType.CANCEL) == buttonYes;
  }
}
