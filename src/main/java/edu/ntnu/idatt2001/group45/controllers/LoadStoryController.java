package edu.ntnu.idatt2001.group45.controllers;

import edu.ntnu.idatt2001.group45.scenes.SceneSwitcher;
import javafx.stage.Stage;

public class LoadStoryController {
  Stage primaryStage;
  SceneSwitcher sceneSwitcher;

  public LoadStoryController(Stage primaryStage, SceneSwitcher sceneSwitcher){
    this.primaryStage = primaryStage;
    this.sceneSwitcher = sceneSwitcher;
  }
}
