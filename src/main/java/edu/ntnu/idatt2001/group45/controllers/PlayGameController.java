package edu.ntnu.idatt2001.group45.controllers;

import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.data.Game;
import edu.ntnu.idatt2001.group45.data.Player;
import edu.ntnu.idatt2001.group45.filehandling.GameReader;
import edu.ntnu.idatt2001.group45.scenes.SceneSwitcher;
import java.io.IOException;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Controller for the playGame class. this class gives the buttons behaviour
 */
public class PlayGameController {

  private Stage primaryStage;
  private SceneSwitcher sceneSwitcher;

  /**
   * the constructor for the PlayGameController.
   *
   * @param primaryStage  the primarystage the controller is used in
   * @param sceneSwitcher the sceneswitcher, so it can change stage.
   */
  public PlayGameController(Stage primaryStage, SceneSwitcher sceneSwitcher) {
    this.primaryStage = primaryStage;
    this.sceneSwitcher = sceneSwitcher;
  }

  /**
   * a method that creates the helpPage when the user clicks on the help button.
   */
  public void helpPage() {
    Stage helpStage = new Stage();
    helpStage.setTitle("Path: Help");
    BorderPane root = new BorderPane();
    helpStage.initOwner(primaryStage);
    helpStage.setResizable(false);

    VBox text = new VBox();
    text.setAlignment(Pos.CENTER);
    text.setSpacing(15);

    Text titleText = new Text("How to play this game!");
    Text helpText = new Text("To play this game, you have to click on the blue links." +
        "\n" + "These links will lead you to a different passage, but be careful," +
        "\n" + "each link will probably have an action that can either be rewarding or punishing," +
        "\n" + "so think before you choose!");
    Text endingText = new Text("Have fun and enjoy this game!");

    helpText.setTextAlignment(TextAlignment.CENTER);

    text.getChildren().addAll(titleText, helpText, endingText);

    root.setCenter(text);
    Scene helpScene = new Scene(root, 450, 250);

    helpStage.setScene(helpScene);

    primaryStage.getScene().getRoot().setDisable(true);

    helpStage.setOnHidden(event -> enablePrimaryStage());
    helpStage.showAndWait();
  }

  public void goBackToMainMenu() {
    sceneSwitcher.switchScene("Main Menu");
  }

  /**
   * a method for creating a pop-up when the game is over. This method creates a new stage, and a
   * layout. The pop-up window gives alternative for the player to restart game or exit back to the
   * main menu.
   *
   * @param text the text that is displayed on the pop-up stage.
   */
  public void GameOver(String text) {
    Stage stage = new Stage();
    stage.setResizable(false);
    BorderPane root = new BorderPane();
    stage.initStyle(StageStyle.UTILITY);
    stage.initModality(Modality.APPLICATION_MODAL);
    stage.initOwner(primaryStage);

    Text gameOverText = new Text(text);

    Text healthGoal = new Text();

    Text goldGoal = new Text();

    Text scoreGoal = new Text();

    Text inventoryGoal = new Text();

    Player player = DataModel.getInstance().getPlayer();

    healthGoal.setText(
        "Health Goal: " + player.getHealth() + "/" + DataModel.getInstance().getHealthGoal()
            .getMinimumHealth());
    goldGoal.setText("Gold Goal: " + player.getGold() + "/" + DataModel.getInstance().getGoldGoal()
        .getMinimumGold());
    scoreGoal.setText(
        "Score Goal: " + player.getScore() + "/" + DataModel.getInstance().getScoreGoal()
            .getMinimumScore());
    inventoryGoal.setText(
        "Inventory Goal: " + DataModel.getInstance().getInventoryGoal().getMandatoryItems()
            .stream().filter(player.getInventory()::contains).count()
            + "/" + DataModel.getInstance().getInventoryGoal().getMandatoryItems().size());

    Button restartButton = new Button("Restart");
    restartButton.setPrefWidth(80);

    restartButton.setOnAction(actionEvent -> {
      restartGame();
      enablePrimaryStage();
      stage.close();
    });

    Button mainMenuButton = new Button("Exit");
    mainMenuButton.setPrefWidth(80);

    mainMenuButton.setOnAction(actionEvent -> {
      DataModel.getInstance().resetDataModel();
      enablePrimaryStage();
      sceneSwitcher.switchScene("Main Menu");
      stage.close();
    });

    HBox buttonContainer = new HBox(10);
    buttonContainer.setPrefHeight(50);
    buttonContainer.setAlignment(Pos.TOP_CENTER);
    buttonContainer.getChildren().addAll(restartButton, mainMenuButton);
    buttonContainer.setPadding(new Insets(10));

    root.setBottom(buttonContainer);
    VBox textContainer = new VBox(10);
    textContainer.setPrefHeight(50);
    textContainer.setPadding(new Insets(10, 10, 10, 10));
    textContainer.setAlignment(Pos.CENTER);
    textContainer.getChildren()
        .addAll(gameOverText, healthGoal, goldGoal, scoreGoal, inventoryGoal);
    root.setCenter(textContainer);
    gameOverText.setTextAlignment(TextAlignment.CENTER);

    Scene gameOverScene = new Scene(root);
    stage.setScene(gameOverScene);

    primaryStage.getScene().getRoot().setDisable(true);
    stage.setOnHidden(event -> enablePrimaryStage());

    stage.setOnCloseRequest(event -> {
      event.consume();
    });

    stage.showAndWait();
  }

  /**
   * a method for enable the primary stage whenever the popup stage is closed
   */
  private void enablePrimaryStage() {
    // Enable the primary stage when the popup stage is closed
    primaryStage.getScene().getRoot().setDisable(false);
  }


  /**
   * a method for returning to the main menu from the game scene.
   */
  public void exitAndSaveGame() {
    sceneSwitcher.switchScene("Main Menu");
  }

  /**
   * method to restart game. This method reads the gameFile that is saved to the DataModel instantly
   * after the all the attributes has been initiated. The dataModel is setting the initial values of
   * the game, when this method is called.
   */
  public void restartGame() {
    GameReader gameReader = new GameReader();
    try {
      Game game = gameReader.readGameFromFile(
          DataModel.getInstance().getGameFile().getAbsolutePath());
      DataModel.getInstance().setGame(game);
      DataModel.getInstance().setPlayer(game.getPlayer());
      DataModel.getInstance().setGoals(game.getGoals());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}

