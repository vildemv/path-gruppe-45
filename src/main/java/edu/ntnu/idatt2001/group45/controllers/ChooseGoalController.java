package edu.ntnu.idatt2001.group45.controllers;

import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.data.Game;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import edu.ntnu.idatt2001.group45.data.goals.GoldGoal;
import edu.ntnu.idatt2001.group45.data.goals.HealthGoal;
import edu.ntnu.idatt2001.group45.data.goals.InventoryGoal;
import edu.ntnu.idatt2001.group45.data.goals.ScoreGoal;
import edu.ntnu.idatt2001.group45.filehandling.GameWriter;
import edu.ntnu.idatt2001.group45.filehandling.GoalWriter;
import edu.ntnu.idatt2001.group45.scenes.SceneSwitcher;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The controller for the ChooseGoalClass.
 */
public class ChooseGoalController {

  private Stage primaryStage;
  private SceneSwitcher sceneSwitcher;

  /**
   * the constructor for the ChooseGoalController.
   *
   * @param primaryStage  the stage
   * @param sceneSwitcher the sceneSwitcher
   */
  public ChooseGoalController(Stage primaryStage, SceneSwitcher sceneSwitcher) {
    this.primaryStage = primaryStage;
    this.sceneSwitcher = sceneSwitcher;
  }

  /**
   * a method to return to the chooseCharacterScene if the user regrets their choice.
   */
  public void onHyperLinkClick() {
    sceneSwitcher.switchScene("Choose Character");
  }

  /**
   * a method for setting the gaols into the DataModel.
   *
   * @param goals the list of goals that will be iterated through and sat to the dataModel.
   */
  public void setGoalToDataModel(List<Goal> goals) {
    for (Goal goal : goals) {
      if (goal instanceof GoldGoal) {
        DataModel.getInstance().setGoldGoal((GoldGoal) goal);
      }
      if (goal instanceof HealthGoal) {
        DataModel.getInstance().setHealthGoal((HealthGoal) goal);
      }
      if (goal instanceof ScoreGoal) {
        DataModel.getInstance().setScoreGoal((ScoreGoal) goal);
      }
      if (goal instanceof InventoryGoal) {
        DataModel.getInstance().setInventoryGoal((InventoryGoal) goal);
      }
    }
    Game game = new Game(DataModel.getInstance().getPlayer(), DataModel.getInstance()
        .getStory(), DataModel.getInstance().getGoals());
    DataModel.getInstance().setGame(game);
    GameWriter gameWriter = new GameWriter();
    File file = gameWriter.saveGameToFile(game);
    DataModel.getInstance().setGameFile(file);
    sceneSwitcher.switchScene("Play Game");
  }

  /**
   * a method that initiates a new Stage where the user can create a new Goal.
   */
  public void createGoals() {
    Stage goalSetStage = new Stage();
    goalSetStage.setTitle("Create Goal Set");
    goalSetStage.setResizable(false);
    goalSetStage.initOwner(primaryStage);

    List<Goal> goals = new ArrayList<>();

    // Create UI controls for goal inputs
    TextField goldGoalTextField = new TextField();
    TextField scoreGoalTextField = new TextField();
    TextField inventoryGoalTextField = new TextField();
    TextField healthGoalTextField = new TextField();

    FlowPane inventoryItemsContainer = new FlowPane();

    inventoryItemsContainer.setHgap(5);
    inventoryItemsContainer.setVgap(5);
    inventoryItemsContainer.setPadding(new Insets(5));

    List<String> inventoryGoalValue = new ArrayList<>();

    Button addItemButton = new Button("+");
    addItemButton.setOnAction(event -> {
      String item = inventoryGoalTextField.getText().trim();
      if (!item.isEmpty()) {
        Label itemLabel = new Label(item);
        inventoryItemsContainer.getChildren().add(itemLabel);
        inventoryGoalValue.add(item);
        inventoryGoalTextField.clear();

        Button removeButton = new Button("-");
        removeButton.setOnAction(removeEvent -> {
          inventoryItemsContainer.getChildren().removeAll(itemLabel, removeButton);
          inventoryGoalValue.remove(item);
        });

        inventoryItemsContainer.getChildren().addAll(removeButton);

        // Clear the text field for the next item
        inventoryGoalTextField.clear();
      }

    });

    Button continueButton = new Button("Continue");
    continueButton.setOnAction(event -> {
      int goldGoalValue = 0;
      int scoreGoalValue = 0;
      int healthGoalValue = 0;
      try {
        goldGoalValue = Integer.parseInt(goldGoalTextField.getText());
      } catch (IllegalArgumentException exception) {
        showErrorAlert("Please enter a valid numeric number for the gold goal!");
        return;
      }
      try {
        scoreGoalValue = Integer.parseInt(scoreGoalTextField.getText());
      } catch (IllegalArgumentException exception) {
        showErrorAlert("Please enter a valid numeric number for the score goal!");
        return;
      }
      try {
        healthGoalValue = Integer.parseInt(healthGoalTextField.getText());
      } catch (IllegalArgumentException exception) {
        showErrorAlert("Please enter a valid numeric number for the health goal!");
        return;
      }

      try {
        initiateGoals(goals, inventoryGoalValue, goldGoalValue, scoreGoalValue, healthGoalValue);
        goalSetStage.close();
        sceneSwitcher.switchScene("Choose Goals");
      } catch (IllegalArgumentException e) {
        showErrorAlert(e.getMessage());
      }

      enablePrimaryStage();
    });
    VBox layout = new VBox(10);
    layout.setPrefSize(400, 450);
    layout.setAlignment(Pos.CENTER);
    layout.setPadding(new Insets(10,10,10,10));
    layout.getChildren().addAll(
        new Label("Gold Goal:"), goldGoalTextField,
        new Label("Score Goal:"), scoreGoalTextField,
        new Label("Health Goal:"), healthGoalTextField,
        new Label("Inventory Goal (add items):"), inventoryGoalTextField, addItemButton,
        new Label("Added Items:"), inventoryItemsContainer, continueButton);

    // Create a scene and set it on the stage
    Scene goalSetScene = new Scene(layout);
    goalSetStage.setScene(goalSetScene);
    primaryStage.getScene().getRoot().setDisable(true);
    goalSetStage.setOnHidden(event -> enablePrimaryStage());
    goalSetStage.showAndWait();
  }

  /**
   * A method that takes in user input from the createGoals stage, and write a .goal file
   *
   * @param goals              the goals
   * @param inventoryGoalValue the InventoryGoal
   * @param goldGoalValue      the GoldGoal
   * @param scoreGoalValue     the scoreGoal
   * @param healthGoalValue    the healthGoal
   */
  private void initiateGoals(List<Goal> goals, List<String> inventoryGoalValue, int goldGoalValue,
      int scoreGoalValue, int healthGoalValue) {
    GoldGoal goldGoal = new GoldGoal(goldGoalValue);
    ScoreGoal scoreGoal = new ScoreGoal(scoreGoalValue);
    InventoryGoal inventoryGoal = new InventoryGoal(inventoryGoalValue);
    HealthGoal healthGoal = new HealthGoal(healthGoalValue);

    DataModel.getInstance().setInventoryGoal(inventoryGoal);
    DataModel.getInstance().setHealthGoal(healthGoal);
    DataModel.getInstance().setScoreGoal(scoreGoal);
    DataModel.getInstance().setGoldGoal(goldGoal);

    goals.add(goldGoal);
    goals.add(scoreGoal);
    goals.add(inventoryGoal);
    goals.add(healthGoal);
    GoalWriter goalWriter = new GoalWriter();
    goalWriter.saveGoalsToFile(goals);
    DataModel.getInstance().setGoals(goals);
  }

  /**
   * a method to call an error message, if the program catches an exception.
   *
   * @param errorMessage the error message
   */
  private void showErrorAlert(String errorMessage) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText(null);
    alert.setContentText(errorMessage);
    alert.showAndWait();
  }

  /**
   * a method for enabling the primary stage when the popup-stage is closed
   */
  private void enablePrimaryStage() {
    primaryStage.getScene().getRoot().setDisable(false);
  }
}
