package edu.ntnu.idatt2001.group45.controllers;

import edu.ntnu.idatt2001.group45.application.DataModel;
import edu.ntnu.idatt2001.group45.data.Story;
import edu.ntnu.idatt2001.group45.filehandling.PathFileReader;
import edu.ntnu.idatt2001.group45.scenes.SceneSwitcher;
import java.io.File;
import javafx.scene.control.Alert;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * The controller for the NewStoryScene. It controls and gives behavior for the buttons in this
 * scene.
 */
public class NewStoryController {

  Stage primaryStage;
  SceneSwitcher sceneSwitcher;

  /**
   * the constructor for the NewStoryController.
   *
   * @param primaryStage  the stage
   * @param sceneSwitcher the sceneswitcher
   */
  public NewStoryController(Stage primaryStage, SceneSwitcher sceneSwitcher) {
    this.primaryStage = primaryStage;
    this.sceneSwitcher = sceneSwitcher;
  }

  /**
   * a method for switching the scene to the premadeStoryScene.
   */
  public void onButtonClick1() {
    sceneSwitcher.switchScene("Premade Story");
  }

  /**
   * a method for the user to upload their own story. The method creates a fileChooser and filters
   * for the user to only choose .path file. When the has chosen a file, the pathFileReader reads
   * the file and returns a story. The story is saved to the DataModel and the user is sent to the
   * next scene.
   */
  public void onButtonClick2() {
    // Create a FileChooser
    FileChooser fileChooser = new FileChooser();

    // Set the initial directory
    fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));

    PathFileReader pathFileReader = new PathFileReader();
    FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Path file",
        "*.path");
    fileChooser.getExtensionFilters().add(extensionFilter);

    File selectedFile = fileChooser.showOpenDialog(primaryStage);

    if (selectedFile != null) {
      try {
        Story story = pathFileReader.readPathFile(selectedFile.getAbsolutePath());
        DataModel.getInstance().setStory(story);
        DataModel.getInstance().setFile(selectedFile);
        sceneSwitcher.switchScene("Choose Character");
      } catch (IllegalArgumentException | NullPointerException e) {
        showErrorAlert();
      }
    }
  }

  /**
   * a method for returning to the main menu.
   */
  public void onHyperlinkClick1() {
    sceneSwitcher.switchScene("Main Menu");
  }

  /**
   * A method to display error if the user uploads an invalid .path file.
   */
  private void showErrorAlert() {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText(null);
    alert.setContentText("Invalid format in the .path file!");
    alert.showAndWait();
  }
}

