package edu.ntnu.idatt2001.group45.data;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * The story class. This class contains passages, which is a map of Passages with links as keys.
 */
public class Story {

  private final String title;
  private final Map<Link, Passage> passages;
  private final Passage openingPassage;

  /**
   * the constructor of the story.
   *
   * @param title          the title of the story
   * @param openingPassage the opening passage
   */
  public Story(String title, Passage openingPassage) {
    this.title = title;
    this.openingPassage = openingPassage;
    passages = new LinkedHashMap<>();
  }

  /**
   * a method to get the title of the story.
   *
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * a method to get the opening passage.
   *
   * @return the opening passage
   */
  public Passage getOpeningPassage() {
    return openingPassage;
  }

  /**
   * a method to add passage to the story.
   *
   * @param passage an already existing passage
   */
  public void addPassage(Passage passage) {
    passages.put(new Link(passage.getTitle(), passage.getTitle()), passage);
  }

  /**
   * a method to remove passages that has links that leads to nowhere.
   *
   * @param link the link to the passage.
   */
  public void removePassage(Link link) {
    boolean linksToPassage = getPassages()
        .stream()
        .flatMap(p -> p.getLinks().stream())
        .anyMatch(l -> l.getReference().equals(link.getReference()));

    if (linksToPassage) {
      throw new IllegalArgumentException("The passage is linked to other passages");
    } else {
      Optional<Link> passageToRemove = passages.keySet()
          .stream()
          .filter(k -> k.getReference().equals(link.getReference()))
          .findFirst();

      passageToRemove.ifPresent(passages::remove);
    }
  }

  /**
   * a method to get broken link. A broken link is a link that leads to no passage.
   *
   * @return returns a list of links.
   */
  public List<Link> getBrokenLinks() {
    List<String> validReference = getPassages()
        .stream()
        .map(Passage::getTitle)
        .toList();

    return getPassages()
        .stream()
        .flatMap(p -> p.getLinks().stream())
        .filter(l -> !validReference.contains(l.getReference()))
        .toList();
  }

  /**
   * a method to get the passage.
   *
   * @param link that are connected to the passage
   * @return returns the passage
   */
  public Passage getPassage(Link link) {
    for (Passage passage : getPassages()) {
      if (link.getReference().equals(passage.getTitle())) {
        return passage;
      }
    }
    return null;
  }

  /**
   * a method to get all the passages in the story.
   *
   * @return returns the passages
   */
  public Collection<Passage> getPassages() {
    return passages.values();
  }

  /**
   * toString for the Story. The toString has been formatted to a specific format that can be used
   * to read or write story for paths file. The toString is used to stringify the Story object.
   *
   * @return returns a string.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(title).append("\n\n");

    for (Passage passage : passages.values()) {
      sb.append(passage).append("\n");
    }

    return sb.toString();
  }

  /**
   * a method to check if a link is broken.
   *
   * @param linkToCheck the link that is checked
   * @return returns true or false, whether it is broken or not.
   */
  public boolean brokenLink(Link linkToCheck) {
    return getBrokenLinks().stream()
        .anyMatch(link -> link.getText().equals(linkToCheck.getText()));
  }

  /**
   * a method to get a list of broken links into a string.
   *
   * @return returns a string of broken links, or no broken links if there are no broken links.
   */
  public String brokenLinksToString() {
    if (!getBrokenLinks().isEmpty()) {
      return getBrokenLinks()
          .stream()
          .map(l -> "[ " + l.getText() + " ]")
          .collect(Collectors.joining(" , "));
    } else {
      return "No broken links";
    }
  }
}