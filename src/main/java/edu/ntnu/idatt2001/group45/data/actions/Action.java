package edu.ntnu.idatt2001.group45.data.actions;

import edu.ntnu.idatt2001.group45.data.Player;

/**
 * An action interface. this interface execute a certain action to the player
 */
public interface Action {

  /**
   * an abstract method that execute a certain action
   *
   * @param player the player
   */
  public void execute(Player player);
}
