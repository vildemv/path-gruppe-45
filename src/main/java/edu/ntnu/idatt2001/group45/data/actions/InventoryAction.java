package edu.ntnu.idatt2001.group45.data.actions;

import edu.ntnu.idatt2001.group45.data.Player;

/**
 * InventoryAction executes an actions that adds an item to the player-inventory.
 */
public class InventoryAction implements Action {

  private final String item;

  /**
   * the constructor for the InventoryAction.
   *
   * @param item the item
   */
  public InventoryAction(String item) {
    this.item = item;
  }

  /**
   * the method that executes the actions that adds item to inventory.
   *
   * @param player the player
   */
  @Override
  public void execute(Player player) {
    player.addToInventory(this.item);
  }
}
