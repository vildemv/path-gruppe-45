package edu.ntnu.idatt2001.group45.data.goals;

import edu.ntnu.idatt2001.group45.data.Player;

/**
 * a class that gives out health requirements for the player.
 */
public class HealthGoal implements Goal {

  private final int minimumHealth;

  /**
   * the constructor for the class.
   *
   * @param minimumHealth the minimum health requirements for the player
   */
  public HealthGoal(int minimumHealth) {
    if (minimumHealth < 0) {
      throw new IllegalArgumentException("The health goal amount must be positive!");
    }
    this.minimumHealth = minimumHealth;
  }

  /**
   * a method that checks if the player met the health requirements.
   *
   * @param player the player you want to check
   * @return returns true or false whether the player met the requirements
   */
  @Override
  public Boolean isFulfilled(Player player) {
    return player.getHealth() > this.minimumHealth;
  }

  public int getMinimumHealth() {
    return minimumHealth;
  }
}
