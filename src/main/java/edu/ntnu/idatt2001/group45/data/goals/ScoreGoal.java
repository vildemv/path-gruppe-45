package edu.ntnu.idatt2001.group45.data.goals;

import edu.ntnu.idatt2001.group45.data.Player;

/**
 * a class that gives out the score requirements for the player.
 */
public class ScoreGoal implements Goal {

  private final int minimumScore;

  /**
   * the constructor of the class.
   *
   * @param minimumScore the minimum score requirements
   */
  public ScoreGoal(int minimumScore) {
    if (minimumScore < 0) {
      throw new IllegalArgumentException("The score goal amount must be positive!");
    }
    this.minimumScore = minimumScore;
  }

  /**
   * a method that checks if the player has the requirements of points to continue.
   *
   * @param player the player you want to check
   * @return true or false whether the player has met the requirements or not
   */
  @Override
  public Boolean isFulfilled(Player player) {
    return player.getScore() > this.minimumScore;
  }

  public int getMinimumScore() {
    return minimumScore;
  }
}
