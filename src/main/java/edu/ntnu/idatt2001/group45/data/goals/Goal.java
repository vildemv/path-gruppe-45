package edu.ntnu.idatt2001.group45.data.goals;

import edu.ntnu.idatt2001.group45.data.Player;

/**
 * an interface for goals that player are required to meet
 */
public interface Goal {

  /**
   * abstract method that checks if the player has met certain requirements
   *
   * @param player the player you want to check
   * @return returns a true or false, whether the player has met their goal
   */
  public abstract Boolean isFulfilled(Player player);
}
