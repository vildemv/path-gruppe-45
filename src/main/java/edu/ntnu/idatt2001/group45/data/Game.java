package edu.ntnu.idatt2001.group45.data;

import edu.ntnu.idatt2001.group45.data.actions.Action;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import java.util.List;

/**
 * the game class. This class contains the player, story and the goals the player has to do
 */
public class Game {

  private Player player;
  private Story story;
  private List<Goal> goals;

  /**
   * the constructor for the game class.
   *
   * @param player the player
   * @param story  the story
   * @param goals  the goals
   */
  public Game(Player player, Story story, List<Goal> goals) {
    this.player = player;
    this.story = story;
    this.goals = goals;
  }

  /**
   * returns the player.
   *
   * @return the player
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * returns the story.
   *
   * @return the story
   */
  public Story getStory() {
    return story;
  }

  /**
   * returns the goals.
   *
   * @return the goals
   */
  public List<Goal> getGoals() {
    return goals;
  }

  /**
   * a method to start the game, and lead the player to the opening passage.
   *
   * @return returns the opening passage
   */

  public Passage begin() {
    return story.getOpeningPassage();
  }

  /**
   * a method to go a certain passage inside the story.
   *
   * @param link the link that are linked to the passage
   * @return returns the passage
   */
  public Passage go(Link link) {
    List<Action> actions = link.getActions();
    for (Action action : actions) {
      action.execute(player);
    }
    return story.getPassage(link);
  }
}
