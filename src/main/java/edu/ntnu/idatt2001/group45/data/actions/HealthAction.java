package edu.ntnu.idatt2001.group45.data.actions;

import edu.ntnu.idatt2001.group45.data.Player;

/**
 * This class add healths to the player.
 */
public class HealthAction implements Action {

  private final int health;

  /**
   * the constructor for the class.
   *
   * @param health the health action value
   */
  public HealthAction(int health) {
    this.health = health;
  }

  /**
   * a method that executes an action that adds health.
   *
   * @param player the player
   */
  @Override
  public void execute(Player player) {
    if (this.health < 0) {
      player.removeHealth(this.health);
    }
    if (this.health > 0) {
      player.addHealth(this.health);
    }
  }
}
