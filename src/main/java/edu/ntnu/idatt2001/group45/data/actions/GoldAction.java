package edu.ntnu.idatt2001.group45.data.actions;

import edu.ntnu.idatt2001.group45.data.Player;

/**
 * GoldAction is a class that adds gold to the player.
 */
public class GoldAction implements Action {

  private final int gold;

  /**
   * the constructor for the class.
   *
   * @param gold the amount of gold
   */
  public GoldAction(int gold) {
    this.gold = gold;
  }

  /**
   * a method that executes an action of adding gold to the player.
   *
   * @param player the player
   */
  @Override
  public void execute(Player player) {
    if (this.gold < 0) {
      player.removeGold(gold);
    }
    if (this.gold > 0) {
      player.addGold(this.gold);
    }
  }
}
