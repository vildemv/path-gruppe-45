package edu.ntnu.idatt2001.group45.data.goals;

import edu.ntnu.idatt2001.group45.data.Player;

/**
 * this class gives player a gold requirement.
 */
public class GoldGoal implements Goal {

  private final int minimumGold;

  /**
   * a constructor for this class.
   *
   * @param minimumGold the minimum gold that is required for the player
   */
  public GoldGoal(int minimumGold) {
    if (minimumGold < 0) {
      throw new IllegalArgumentException("The gold goal amount must be positive!");
    }
    this.minimumGold = minimumGold;
  }

  /**
   * a method that checks if player gold has more gold than it is required.
   *
   * @param player the player you want to check
   * @return a boolean whether the player has more or less than the requirement
   */
  @Override
  public Boolean isFulfilled(Player player) {
    return player.getGold() > this.minimumGold;
  }

  public int getMinimumGold() {
    return minimumGold;
  }
}
