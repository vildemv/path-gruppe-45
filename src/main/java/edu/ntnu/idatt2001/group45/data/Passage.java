package edu.ntnu.idatt2001.group45.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Passage is a class that have a title and some content inside it. The passage has a list of links
 */
public class Passage {

  private final String title;
  private final String content;
  private final List<Link> links;

  /**
   * the constructor for the Passage.
   *
   * @param title   the title of the passage
   * @param content the content inside the passage
   */
  public Passage(String title, String content) {
    this.title = title;
    this.content = content;
    links = new ArrayList<>();
  }

  /**
   * a getter for the title.
   *
   * @return returns the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * a getter for the content.
   *
   * @return returns the content
   */

  public String getContent() {
    return content;
  }

  /**
   * a method to add link.
   *
   * @param link is the link
   * @return returns a boolean whether it succeeded or not
   */
  public boolean addLink(Link link) {
    if (link != null) {
      links.add(link);
      return true;
    } else {
      return false;
    }
  }

  /**
   * a method to get all the links.
   *
   * @return returns a list of the links
   */
  public List<Link> getLinks() {
    return links;
  }

  /**
   * a boolean method to check if a passage has links.
   *
   * @return returns true or false whether there are links
   */
  public boolean hasLinks() {
    return links.size() > 0;
  }

  /**
   * check if the passage is equal to the other passage.
   *
   * @param o object you want to compare with
   * @return returns either true or false whether they're equal
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Passage passage)) {
      return false;
    }
    return title.equals(passage.title) && content.equals(passage.content) && links.equals(
        passage.links);
  }

  /**
   * hashcode method.
   *
   * @return returns the hashcode of the object
   */
  @Override
  public int hashCode() {
    return Objects.hash(title, content, links);
  }

  /**
   * giving the Passage object string properties. The passage is stringify to match a certain
   * passage format.
   *
   * @return returns a string.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("::").append(getTitle()).append("\n")
        .append(getContent());
    for (Link link : getLinks()) {
      sb.append(link).append("\n");
    }

    return sb.toString();
  }
}