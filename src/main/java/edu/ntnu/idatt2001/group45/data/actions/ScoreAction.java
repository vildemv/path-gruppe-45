package edu.ntnu.idatt2001.group45.data.actions;

import edu.ntnu.idatt2001.group45.data.Player;

/**
 * This class adds score to the player.
 */
public class ScoreAction implements Action {

  private final int points;

  /**
   * the constructor of this class.
   *
   * @param points the amount of points
   */
  public ScoreAction(int points) {
    this.points = points;
  }

  /**
   * this method executes the actions so.
   *
   * @param player the player
   */
  @Override
  public void execute(Player player) {
    if (this.points < 0) {
      player.removeScore(this.points);
    }
    if (this.points > 0) {
      player.addScore(this.points);
    }
  }


}
