package edu.ntnu.idatt2001.group45.data;

import java.util.ArrayList;
import java.util.List;

/**
 * a player class for the player.
 */

// we should make modify so it's only possible to remove possible integers, but we'll fix it later
public class Player {

  private final String name;
  private int health;
  private int score;
  private int gold;
  private final List<String> inventory;

  /**
   * the constructor for the player, the player has a name, health,score and an inventory.
   *
   * @param name   the name of the player
   * @param health the health of the player
   * @param score  the score of the player
   * @param gold   the gold of the player
   */
  public Player(String name, int health, int score, int gold) {
    if (name.isBlank()) {
      throw new IllegalArgumentException("The name can not be empty");
    } else {
      this.name = name;
    }
    if (health < 0) {
      throw new IllegalArgumentException("The health can not be below zero");
    } else {
      this.health = health;
    }
    if (score < 0) {
      throw new IllegalArgumentException("The score can not be below zero");
    } else {
      this.score = score;
    }
    if (gold < 0) {
      throw new IllegalArgumentException("The gold can not be below zero");
    } else {
      this.gold = gold;
    }
    //initiating inventory as an arraylist or else the inventory will be null
    inventory = new ArrayList<>();
  }

  /**
   * The builder class is used to build the player object with different combinations of
   * attributes.
   *
   * @param builder the Builder object used to build the player.
   */
  private Player(Builder builder) {
    this.name = builder.name;
    this.health = builder.health;
    this.score = builder.score;
    this.gold = builder.gold;
    this.inventory = new ArrayList<>(builder.inventory);
  }

  /**
   * The Builder class that is used to build a player.
   */
  public static class Builder {

    private final String name;
    private int health;
    private int gold;
    private int score;
    private List<String> inventory;

    /**
     * Constructs a new Builder instance with a specified name. All the other attributes will be
     * defined as a standard value, if it isn't specified.
     *
     * @param name the name of the player
     * @throws IllegalArgumentException if the player isn't given a name.
     */
    public Builder(String name) {
      if (name.isEmpty()) {
        throw new IllegalArgumentException("You must give the player a name!");
      } else {
        this.name = name;
      }
      this.health = 100;
      this.gold = 150;
      this.score = 0;
      this.inventory = new ArrayList<>();
    }

    /**
     * sets the health of the player.
     *
     * @param health the health value
     * @return returns the Builder object
     * @throws IllegalArgumentException if the health value is zero or under
     */
    public Builder health(int health) {
      if (health <= 0) {
        throw new IllegalArgumentException("The player cannot have zero health!");
      } else {
        this.health = health;
      }
      return this;
    }

    /**
     * sets the score of the palyer
     *
     * @param score the score value
     * @return returns the Builder object
     * @throws IllegalArgumentException if the score value is zero
     */
    public Builder score(int score) {
      if (score < 0) {
        throw new IllegalArgumentException("The score can not be below zero");
      } else {
        this.score = score;
      }
      return this;
    }

    /**
     * sets the gold of the player.
     *
     * @param gold the gold value
     * @return returns the Builder object
     * @throws IllegalArgumentException if the gold value is zero
     */
    public Builder gold(int gold) {
      if (gold < 0) {
        throw new IllegalArgumentException("The gold can not be below zero");
      } else {
        this.gold = gold;
      }
      return this;
    }

    /**
     * adds an item to teh player's inventory.
     *
     * @param item the item that is added
     * @return returns the Builder object
     */
    public Builder inventory(String item) {
      this.inventory.add(item);
      return this;
    }

    /**
     * Constructs a new Player object with the specified attributes.
     *
     * @return the new Player object
     */
    public Player build() {
      return new Player(this);
    }
  }

  /**
   * a method to get the name of the player.
   *
   * @return returns the name of the player
   */
  public String getName() {
    return this.name;
  }

  /**
   * a method to add health to the player.
   *
   * @param health the positive amount of health
   */
  public void addHealth(int health) {
    if (health < 0) {
      throw new IllegalArgumentException("this is not possible");
    }
    this.health += health;
  }

  /**
   * a method to remove health.
   *
   * @param health positive amount of health
   */

  public void removeHealth(int health) {
    health = -health;
    if (health > this.health) {
      this.health = 0;
    } else {
      this.health -= health;
    }
  }

  /**
   * a method to get health of the player.
   *
   * @return the amount of health the player has
   */

  public int getHealth() {
    return this.health;
  }

  /**
   * a method to add score to the player.
   *
   * @param points a positive amount of score
   */
  public void addScore(int points) {
    if (score < 0) {
      throw new IllegalArgumentException("this is not possible");
    } else {
      this.score += points;
    }

  }

  /**
   * a method to remove score.
   *
   * @param points the amount of points
   */
  public void removeScore(int points) {
    points = -points;
    if (points > this.score) {
      this.score = 0;
    } else {
      this.gold -= gold;
    }
  }

  /**
   * a method to get score to the player.
   *
   * @return returns the score
   */
  public int getScore() {
    return this.score;
  }

  /**
   * a method to add gold to the player.
   *
   * @param gold the amount of gold
   */
  public void addGold(int gold) {
    if (gold < 0) {
      throw new IllegalArgumentException("this is not possible");
    } else {
      this.gold += gold;
    }
  }

  /**
   * a method to remove the gold.
   *
   * @param gold the amount of gold
   */
  //adding a method for removing gold
  public void removeGold(int gold) {
    gold = -gold;
    if (gold > this.gold) {
      this.gold = 0;
    } else {
      this.gold -= gold;
    }
  }

  /**
   * a method to see how much gold the player has.
   *
   * @return returns the amount of gold the player has
   */
  public int getGold() {
    return this.gold;
  }

  /**
   * a method to add item to inventory.
   *
   * @param item the item that is added
   */
  public void addToInventory(String item) {
    inventory.add(item);
  }

  /**
   * a method to get the playerinventory.
   *
   * @return returns the inventory
   */
  public List<String> getInventory() {
    return inventory;
  }

  /**
   * a method for making the player displayable as a string.
   *
   * @return returns the String of the player.
   */
  @Override
  public String toString() {
    return "Player{" +
        "name:" + name +
        ", health:" + health +
        ", score:" + score +
        ", gold:" + gold +
        ", inventory:" + inventory +
        '}';
  }
}
