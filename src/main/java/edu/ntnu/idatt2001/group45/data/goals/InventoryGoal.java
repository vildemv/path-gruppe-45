package edu.ntnu.idatt2001.group45.data.goals;

import edu.ntnu.idatt2001.group45.data.Player;
import java.util.List;

/**
 * This class gives player item requirements in order to continue the game.
 */
public class InventoryGoal implements Goal {

  private final List<String> mandatoryItems;

  /**
   * the constructor of the class.
   *
   * @param mandatoryItems a list of items
   */
  public InventoryGoal(List<String> mandatoryItems) {
    this.mandatoryItems = mandatoryItems;
  }

  /**
   * a method that checks if the inventory contains vital items.
   *
   * @param player the player
   * @return returns either true or false
   */
  @Override
  public Boolean isFulfilled(Player player) {
    return player.getInventory().containsAll(mandatoryItems);
  }

  public List<String> getMandatoryItems() {
    return mandatoryItems;
  }
}
