package edu.ntnu.idatt2001.group45.data;

import edu.ntnu.idatt2001.group45.data.actions.Action;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * the link class. This class connects to a passage
 */
public class Link {

  private final String text;
  private final String reference;
  private final List<Action> actions;

  /**
   * the constructor of the link class.
   *
   * @param text      the text of the link
   * @param reference the reference of the link
   */
  public Link(String text, String reference) {
    this.text = text;
    this.reference = reference;
    actions = new ArrayList<>();
  }

  /**
   * a method to get text.
   *
   * @return returns the text
   */
  public String getText() {
    return this.text;
  }

  /**
   * a method to get reference.
   *
   * @return returns the reference of the link
   */
  public String getReference() {
    return this.reference;
  }

  /**
   * a method to add an action in the link.
   *
   * @param action the action
   */
  public void addAction(Action action) {
    actions.add(action);
  }

  /**
   * a method to get all the actions.
   *
   * @return returns a lists of all the actions
   */
  public List<Action> getActions() {
    return actions;
  }

  /**
   * an equals method.
   *
   * @param o the object you want to compare
   * @return a boolean whether they are equal or not
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Link link = (Link) o;
    return Objects.equals(text, link.text) && Objects.equals(reference, link.reference)
        && Objects.equals(actions, link.actions);
  }

  /**
   * toString of the link object.
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "[" + text + "](" + reference + ")";
  }

  /**
   * hashcode for the object.
   *
   * @return the hashcode of the object
   */
  @Override
  public int hashCode() {
    return super.hashCode();
  }

}
