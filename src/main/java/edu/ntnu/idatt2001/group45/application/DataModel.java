package edu.ntnu.idatt2001.group45.application;

import edu.ntnu.idatt2001.group45.data.Game;
import edu.ntnu.idatt2001.group45.data.Passage;
import edu.ntnu.idatt2001.group45.data.Player;
import edu.ntnu.idatt2001.group45.data.Story;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import edu.ntnu.idatt2001.group45.data.goals.GoldGoal;
import edu.ntnu.idatt2001.group45.data.goals.HealthGoal;
import edu.ntnu.idatt2001.group45.data.goals.InventoryGoal;
import edu.ntnu.idatt2001.group45.data.goals.ScoreGoal;
import java.io.File;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * The DataModel class represents the central data model of the application. It holds various
 * properties and objects related to the game, story, player, goals, and more.
 */
public class DataModel {

  private static DataModel instance;
  private Story story;
  private Player player;
  private GoldGoal goldGoal;
  private HealthGoal healthGoal;
  private InventoryGoal inventoryGoal;
  private ObjectProperty<Story> storyProperty;

  private ObjectProperty<Player> playerProperty;
  private ScoreGoal scoreGoal;

  private ObjectProperty<File> fileProperty;

  private File file;

  private ObjectProperty<GoldGoal> goldGoalObjectProperty;

  private ObjectProperty<HealthGoal> healthGoalObjectProperty;

  private ObjectProperty<ScoreGoal> scoreGoalObjectProperty;

  private ObjectProperty<InventoryGoal> inventoryGoalObjectProperty;

  private List<Goal> goals;

  private ObjectProperty<List<Goal>> goalsObject;

  private Game game;

  private ObjectProperty<Game> gameObjectProperty;

  private File gameFile;

  private ObjectProperty<File> gameFileObjectProperty;

  private Passage passage;

  private ObjectProperty<Passage> passageProperty;

  private DataModel() {
    story = null;
    player = null;
    goldGoal = null;
    healthGoal = null;
    scoreGoal = null;
    goals = null;
    game = null;
    passage = null;
    gameObjectProperty = new SimpleObjectProperty<>(null);
    goalsObject = new SimpleObjectProperty<>(null);
    storyProperty = new SimpleObjectProperty<>(null);
    playerProperty = new SimpleObjectProperty<>(null);
    file = null;
    fileProperty = new SimpleObjectProperty<>(null);
    goldGoalObjectProperty = new SimpleObjectProperty<>(null);
    scoreGoalObjectProperty = new SimpleObjectProperty<>(null);
    healthGoalObjectProperty = new SimpleObjectProperty<>(null);
    inventoryGoalObjectProperty = new SimpleObjectProperty<>(null);
    gameFile = null;
    gameFileObjectProperty = new SimpleObjectProperty<>(null);
    passageProperty = new SimpleObjectProperty<>(null);
  }

  /**
   * Returns the singleton instance of the DataModel.
   *
   * @return the DataModel instance
   */
  public static DataModel getInstance() {
    if (instance == null) {
      instance = new DataModel();
    }
    return instance;
  }

  /**
   * sets the current story in the DataModel.
   *
   * @param story the Story object to set
   */
  public void setStory(Story story) {
    this.story = story;
    storyProperty().set(story);
  }

  /**
   * returns the current story in the DataModel.
   *
   * @return the current Story object
   */
  public Story getStory() {
    return story;
  }

  /**
   * sets the current player in the DataModel.
   *
   * @param player the player object to set
   */
  public void setPlayer(Player player) {
    this.player = player;
    playerProperty.set(player);
  }

  /**
   * returns the current player in the DataModel.
   *
   * @return the current Player object
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * sets the current GoldGoal in the DataModel.
   *
   * @param goldGoal the GoldGoal object to set
   */
  public void setGoldGoal(GoldGoal goldGoal) {
    this.goldGoal = goldGoal;
    goldGoalObjectProperty.set(goldGoal);
  }

  /**
   * returns the current GoldGoal in the DataModel.
   *
   * @return the current GoldGoal object;
   */
  public GoldGoal getGoldGoal() {
    return goldGoal;
  }

  /**
   * sets the current HealthGoal in the DataModel.
   *
   * @param healthGoal the HealthGOal object to set
   */
  public void setHealthGoal(HealthGoal healthGoal) {
    this.healthGoal = healthGoal;
    healthGoalObjectProperty.set(healthGoal);
  }

  /**
   * returns the current HealthGoal in the DataModel.
   *
   * @return the current HealthGoal
   */
  public HealthGoal getHealthGoal() {
    return healthGoal;
  }

  /**
   * set the current HealthGoal in the DataModel.
   *
   * @param inventoryGoal the InventoryGoal object to set
   */
  public void setInventoryGoal(InventoryGoal inventoryGoal) {
    this.inventoryGoal = inventoryGoal;
    inventoryGoalObjectProperty.set(inventoryGoal);
  }

  /**
   * returns the current InventoryGoal in the DataModel.
   *
   * @return the current InventoryGoal
   */
  public InventoryGoal getInventoryGoal() {
    return inventoryGoal;
  }

  /**
   * returns the current ScoreGoal in the DataModel.
   *
   * @return the current ScoreGoal
   */
  public ScoreGoal getScoreGoal() {
    return scoreGoal;
  }

  /**
   * sets the current ScoreGoal in the DataModel.
   *
   * @param scoreGoal the ScoreGoal to be set
   */
  public void setScoreGoal(ScoreGoal scoreGoal) {
    this.scoreGoal = scoreGoal;
    scoreGoalObjectProperty.set(scoreGoal);
  }

  /**
   * returns the story property in the DataModel.
   *
   * @return the ObjectProperty representing the player
   */
  public ObjectProperty<Story> storyProperty() {
    return storyProperty;
  }

  /**
   * returns the player property in the DataModel.
   *
   * @return the ObjectProperty representing the player
   */
  public ObjectProperty<Player> playerObjectProperty() {
    return playerProperty;
  }

  /**
   * returns the current pathFile that the user chose to upload in the DataModel.
   *
   * @return the current pathFile
   */
  public File getFile() {
    return file;
  }

  /**
   * sets the current pathFile in the DataModel.
   *
   * @param file the file to be set
   */
  public void setFile(File file) {
    this.file = file;
    fileProperty.set(file);
  }

  /**
   * returns the pathFile property in the DataModel.
   *
   * @return the ObjectProperty representing the pathFile
   */
  public ObjectProperty<File> getFileProperty() {
    return fileProperty;
  }

  /**
   * returns the GoldGoal property in the DataModel.
   *
   * @return the ObjectProperty representing the GoldGoal
   */
  public ObjectProperty<GoldGoal> getGoldGoalObjectProperty() {
    return goldGoalObjectProperty;
  }

  /**
   * returns the ScoreGoal property in the DataModel.
   *
   * @return the ObjectProperty representing the ScoreGoal
   */
  public ObjectProperty<ScoreGoal> getScoreGoalObjectProperty() {
    return scoreGoalObjectProperty;
  }

  /**
   * returns the InventoryGoal property in the DataModel.
   *
   * @return the ObjectProperty representing the InventoryGoal
   */
  public ObjectProperty<InventoryGoal> getInventoryGoalObjectProperty() {
    return inventoryGoalObjectProperty;
  }

  /**
   * returns the HealthGoal property in the DataModel.
   *
   * @return the ObjectProperty representing the HealthGoal
   */
  public ObjectProperty<HealthGoal> getHealthGoalObjectProperty() {
    return healthGoalObjectProperty;
  }

  /**
   * returns the current goals in the DataModel.
   *
   * @return the current goals
   */
  public List<Goal> getGoals() {
    return goals;
  }

  /**
   * sets the current goals in the DataModel.
   *
   * @param goals the goals to be set
   */
  public void setGoals(List<Goal> goals) {
    this.goals = goals;
    goalsObject.set(goals);
  }

  /**
   * returns the goals property.
   *
   * @return ObjectProperty representing goals
   */
  public ObjectProperty<List<Goal>> getGoalsObject() {
    return goalsObject;
  }

  /**
   * returns the current game in the DataModel.
   *
   * @return the current game
   */
  public Game getGame() {
    return game;
  }

  /**
   * sets the current game in the DataModel.
   *
   * @param game the game to be set
   */
  public void setGame(Game game) {
    this.game = game;
    gameObjectProperty.set(game);
  }

  /**
   * returns the game property in the DataModel.
   *
   * @return ObjectProperty representing the game
   */
  public ObjectProperty<Game> getGameObjectProperty() {
    return gameObjectProperty;
  }

  /**
   * returns the current GameFile in the DataModel.
   *
   * @return the current gameFile
   */
  public File getGameFile() {
    return gameFile;
  }

  /**
   * sets the current gameFile in the DataModel.
   *
   * @param gameFile the gameFile to be set
   */
  public void setGameFile(File gameFile) {
    this.gameFile = gameFile;
    gameFileObjectProperty.set(gameFile);
  }

  /**
   * returns the gameFile property in the DataModel.
   *
   * @return ObjectProperty representing the gameFile
   */
  public ObjectProperty<File> getGameFileObjectProperty() {
    return gameFileObjectProperty;
  }

  /**
   * returns the current passage in the DataModel.
   *
   * @return the current passage
   */
  public Passage getPassage() {
    return passage;
  }

  /**
   * sets the current passage in the DataModel.
   *
   * @param passage the passage to set
   */
  public void setPassage(Passage passage) {
    this.passage = passage;
    passageProperty.set(passage);
  }

  /**
   * returns the passage property in the DataModel.
   *
   * @return ObjectProperty that represents the passage
   */
  public ObjectProperty<Passage> getPassageProperty() {
    return passageProperty;
  }

  /**
   * method for resetting the story, goals and player in the DataModel. It doesn't reset everythign
   * but rather the necessary values that is used to create a game.
   */
  public void resetDataModel() {
    DataModel.getInstance().setStory(null);
    DataModel.getInstance().setPlayer(null);
    DataModel.getInstance().setGoals(null);
  }
}