package edu.ntnu.idatt2001.group45.application;

import edu.ntnu.idatt2001.group45.scenes.ChooseCharacterScene;
import edu.ntnu.idatt2001.group45.scenes.ChooseGoalScene;
import edu.ntnu.idatt2001.group45.scenes.MainMenuScene;
import edu.ntnu.idatt2001.group45.scenes.NewStoryScene;
import edu.ntnu.idatt2001.group45.scenes.PlayGameScene;
import edu.ntnu.idatt2001.group45.scenes.PremadeStoryScene;
import edu.ntnu.idatt2001.group45.scenes.SceneSwitcher;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The PrimaryStage class initiates all the scenes and puts in the SceneSwitcher.
 */
public class PrimaryStage extends Application {

  private static final double SCENE_WIDTH = 800;
  private static final double SCENE_HEIGHT = 600;

  public static void main(String[] args) {
    launch(args);
  }

  /**
   * A method for initiating the application. This method creates all the scenes and puts in
   *
   * @param primaryStage the primary stage
   * @throws Exception throws exception if the ChooseCharacter- and the ChooseGoalScene cannot find
   *                   targeted files in the folder
   */
  @Override
  public void start(Stage primaryStage) throws Exception {

    SceneSwitcher sceneSwitcher = new SceneSwitcher(primaryStage);

    MainMenuScene mainMenuScene = new MainMenuScene(primaryStage, sceneSwitcher, SCENE_WIDTH,
        SCENE_HEIGHT);
    NewStoryScene newStoryScene = new NewStoryScene(primaryStage, sceneSwitcher, SCENE_WIDTH,
        SCENE_HEIGHT);
    ChooseCharacterScene chooseCharacterScene = new ChooseCharacterScene(primaryStage,
        sceneSwitcher, SCENE_WIDTH, SCENE_HEIGHT);
    PremadeStoryScene premadeStoryScene = new PremadeStoryScene(primaryStage, sceneSwitcher,
        SCENE_WIDTH, SCENE_HEIGHT);
    ChooseGoalScene chooseGoalScene = new ChooseGoalScene(primaryStage, sceneSwitcher, SCENE_WIDTH,
        SCENE_HEIGHT);
    PlayGameScene playGameScene = new PlayGameScene(primaryStage, sceneSwitcher, SCENE_WIDTH,
        SCENE_HEIGHT);
    sceneSwitcher.addScene("Main Menu", mainMenuScene);
    sceneSwitcher.addScene("New Story", newStoryScene);
    sceneSwitcher.addScene("Premade Story", premadeStoryScene);
    sceneSwitcher.addScene("Choose Character", chooseCharacterScene);
    sceneSwitcher.addScene("Choose Goals", chooseGoalScene);
    sceneSwitcher.addScene("Play Game", playGameScene);
    primaryStage.setResizable(false);
    sceneSwitcher.switchScene("Main Menu");
    primaryStage.show();
  }


}
