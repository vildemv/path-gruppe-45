package edu.ntnu.idatt2001.group45.filehandling;

import edu.ntnu.idatt2001.group45.data.goals.Goal;
import edu.ntnu.idatt2001.group45.data.goals.GoldGoal;
import edu.ntnu.idatt2001.group45.data.goals.HealthGoal;
import edu.ntnu.idatt2001.group45.data.goals.InventoryGoal;
import edu.ntnu.idatt2001.group45.data.goals.ScoreGoal;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * The GoalWriter class is a class that works on writing the Goal-object into a text file. The
 * goalWriter class also includes a method that allows the user to convert the Goal into a String.
 */
public class GoalWriter {

  /**
   * a method for writing a list of goals into a certain text format .goals. In addition, if the
   * method detects that the user doesn't have a goals folder, it creates one and stores the .goal
   * file inside the folder
   *
   * @param goals the list of goals that is going to be written into a .goals file
   * @return returns the .goal file.
   */
  public File saveGoalsToFile(List<Goal> goals) {
    String directoryPath =
        "src" + File.separator + "main" + File.separator + "resources" + File.separator + "goals";
    File directory = new File(directoryPath);
    if (!directory.exists()) {
      directory.mkdirs(); // Create the directory if it doesn't exist
    }
    int sizeOfDirectory = Objects.requireNonNull(directory.listFiles()).length + 1;
    String filePath = directoryPath + File.separator + "goalSet" + sizeOfDirectory + ".goals";
    File file = new File(filePath);
    try (FileWriter writer = new FileWriter(filePath)) {
      for (Goal goal : goals) {
        String goalString = convertGoalToString(goal);
        writer.write(goalString + "\n");
      }

      System.out.println("Player character saved to file: " + directoryPath);
    } catch (IOException e) {
      System.err.println("Failed to save goals to file: " + directoryPath);
      e.printStackTrace();
    }
    return file;
  }

  /**
   * a method to convert Goal into a String. This method is used to simplify the writer method, and
   * make it more readable. The method takes in a goal as an argument and checks if the goal is
   * instance of a certain goal type and returns the goal as a string.
   *
   * @param goal the goal that is going to be converted into a String
   * @return returns a string.
   */
  public String convertGoalToString(Goal goal) {
    if (goal instanceof GoldGoal goldGoal) {
      return "goldGoal=" + goldGoal.getMinimumGold();
    }
    if (goal instanceof HealthGoal healthGoal) {
      return "healthGoal=" + healthGoal.getMinimumHealth();
    }
    if (goal instanceof ScoreGoal scoreGoal) {
      return "ScoreGoal=" + scoreGoal.getMinimumScore();
    }
    if (goal instanceof InventoryGoal inventoryGoal) {
      return "inventoryGoal=" + inventoryGoal.getMandatoryItems();
    } else {
      throw new IllegalArgumentException("Unknown goal type: " + goal.getClass().getSimpleName());
    }
  }
}
