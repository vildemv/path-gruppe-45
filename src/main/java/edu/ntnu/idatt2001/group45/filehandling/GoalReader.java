package edu.ntnu.idatt2001.group45.filehandling;

import edu.ntnu.idatt2001.group45.data.goals.Goal;
import edu.ntnu.idatt2001.group45.data.goals.GoldGoal;
import edu.ntnu.idatt2001.group45.data.goals.HealthGoal;
import edu.ntnu.idatt2001.group45.data.goals.InventoryGoal;
import edu.ntnu.idatt2001.group45.data.goals.ScoreGoal;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * GoalReader class focuses on reading a .goal file and converting it into a Goal Object.
 */
public class GoalReader {

  /**
   * this method primarily focuses on creating a List of goals from a .goal file. This method reads
   * from the file, and creates a certain goalType and adds it to a list.
   *
   * @param filePath the path of the file
   * @return returns the list of the goals.
   */
  public List<Goal> readGoalFromFile(String filePath) {
    List<Goal> goals = new ArrayList<>();
    try (Scanner sc = new Scanner(new File(filePath))) {
      while (sc.hasNextLine()) {
        String line = sc.nextLine();
        Goal goal = convertStringToGoal(line);
        goals.add(goal);
      }
    } catch (IOException e) {
      e.printStackTrace();
      System.err.println("Failed to read goals from file: " + filePath);
    }
    return goals;
  }

  /**
   * a method to convert String into a goal, so it's makes the initial method easier to read. It
   * finds a certain goal pattern to find and create the type of goal from the .goal file, and
   * returns the goal.
   *
   * @param goalString the line from the .goal file.
   * @return returns the converted goal.
   */
  private Goal convertStringToGoal(String goalString) {
    String regex = "(\\w+)=(.+)";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(goalString);

    if (matcher.find()) {
      String goalType = matcher.group(1).toLowerCase();
      String goalAmount = matcher.group(2);

      switch (goalType) {
        case "goldgoal" -> {
          int minimumGold = Integer.parseInt(goalAmount);
          return new GoldGoal(minimumGold);
        }
        case "healthgoal" -> {
          int minimumHealth = Integer.parseInt(goalAmount);
          return new HealthGoal(minimumHealth);
        }
        case "scoregoal" -> {
          int minimumScore = Integer.parseInt(goalAmount);
          return new ScoreGoal(minimumScore);
        }
        case "inventorygoal" -> {
          return new InventoryGoal(getInventoryItems(goalAmount));
        }
        default -> throw new IllegalArgumentException("Unknown goal type: " + goalType);
      }

    } else {
      throw new IllegalArgumentException("Invalid goal format: " + goalString);
    }
  }

  /**
   * a method for reading the inventory goal. Since the inventory goal is different from the other
   * goals, it is used a different pattern to read and convert mandatory items from the .goal file
   * into a list of strings.
   *
   * @param goalAmount the value inside the inventoryGoal
   * @return returns the list of mandatory items.
   */
  private List<String> getInventoryItems(String goalAmount) {
    List<String> mandatoryItems = new ArrayList<>();

    String itemRegex = "\\[([^\\[\\]]*)\\]";
    Pattern itemPattern = Pattern.compile(itemRegex);
    Matcher itemMatcher = itemPattern.matcher(goalAmount);

    if (itemMatcher.find()) {
      String itemGroup = itemMatcher.group(1);
      String[] items = itemGroup.split(",");
      for (String item : items) {
        mandatoryItems.add(item.trim());
      }
    }
    return mandatoryItems;
  }
}
