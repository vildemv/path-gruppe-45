package edu.ntnu.idatt2001.group45.filehandling;

import edu.ntnu.idatt2001.group45.data.Player;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * a method to read the .player format, that is written from the playerWriter class. This class
 * reads the .player format and creates and returns a player object.
 */
public class PlayerReader {

  /**
   * a method for reading the .player file and creating and returning a player object. This method
   * uses matcher and patterns to read a certain pattern from the .player format to initiate values
   * to the player.
   *
   * @param filePath the file path of the player.file.
   * @return returns the player that is created from the .player file.
   * @throws FileNotFoundException returns FileNotFoundException when the filePath is invalid.
   */
  public Player readPlayerFile(String filePath) throws FileNotFoundException {
    try (Scanner sc = new Scanner(new File(filePath))) {

      String name = "";
      int health = 0;
      int gold = 0;
      int score = 0;
      List<String> inventory = new ArrayList<>();

      while (sc.hasNextLine()) {
        String line = sc.nextLine();
        String patternString = "(\\w+):(\\w+|-\\d+)|(\\w+):\\[([^\\[\\]]*)]";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(line);

        while (matcher.find()) {
          if (matcher.group(3) != null) {
            String key = matcher.group(3).toLowerCase();
            if ("inventory".equals(key)) {
              inventory = getInventoryFromRead(line);
            } else {
              throw new IllegalArgumentException
                  ("Error, can't read this player attribute: " + key);
            }
          } else {
            String key = matcher.group(1).toLowerCase();
            String value = matcher.group(2);
            switch (key) {
              case "name" -> name = value;
              case "gold" -> gold = Integer.parseInt(value);
              case "score" -> score = Integer.parseInt(value);
              case "health" -> health = Integer.parseInt(value);
              case default -> throw new IllegalArgumentException(
                  "Error, cant read this player attribute:" + key);
            }
          }

        }

      }
      Player player = new Player.Builder(name).score(score).gold(gold).health(health).build();
      for (String item : inventory) {
        player.addToInventory(item);
      }
      return player;
    }

  }

  /**
   * a private method to read inventory from a certain pattern. This reads the inventory from the
   * player-file and adds it to a list, and when it's done, the method returns the list
   *
   * @param line the line of the scanner.
   * @return the list of the item of the player.
   */
  private List<String> getInventoryFromRead(String line) {
    Pattern pattern = Pattern.compile("(\\w+):\\[([^\\[\\]]*)]");
    Matcher matcher = pattern.matcher(line);
    List<String> inventory = new ArrayList<>();
    if (matcher.find()) {
      String itemGroup = matcher.group(2);
      String[] items = itemGroup.split(",");
      inventory.addAll(Arrays.asList(items));
    }
    return inventory;
  }
}