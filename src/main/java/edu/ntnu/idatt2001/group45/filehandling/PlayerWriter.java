package edu.ntnu.idatt2001.group45.filehandling;

import edu.ntnu.idatt2001.group45.data.Player;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * a class for writing the player stats inside a text file, with the format: .players.
 */
public class PlayerWriter {

  /**
   * a method for writing a player object into a text file.
   *
   * @param player the player that is going to be written
   * @return returns the text-file of the player.
   */
  public File savePlayerToFile(Player player) {
    String directoryPath =
        "src" + File.separator + "main" + File.separator + "resources" + File.separator + "players";
    File directory = new File(directoryPath);
    if (!directory.exists()) {
      directory.mkdirs(); // Create the directory if it doesn't exist
    }
    String filePath = directoryPath + File.separator + player.getName() + ".player";
    File file = new File(filePath);
    try (FileWriter writer = new FileWriter(filePath)) {
      writer.write("Name:" + player.getName() + "\n");
      writer.write("Gold:" + player.getGold() + "\n");
      writer.write("Health:" + player.getHealth() + "\n");
      writer.write("Score:" + player.getScore() + "\n");
      writer.write("Inventory:" + player.getInventory() + "\n");

      System.out.println("Player character saved to file: " + player.getName() + ".player");
    } catch (IOException e) {
      System.err.println(
          "Failed to save player character to file: " + player.getName() + ".player");
      e.printStackTrace();
    }
    return file;
  }
}