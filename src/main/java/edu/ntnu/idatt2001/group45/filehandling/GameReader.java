package edu.ntnu.idatt2001.group45.filehandling;

import edu.ntnu.idatt2001.group45.data.Game;
import edu.ntnu.idatt2001.group45.data.Player;
import edu.ntnu.idatt2001.group45.data.Story;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 * The GameReader class focuses on reading a .game file and creating this game objects. This will
 * allow the user to store games into a folder.
 */
public class GameReader {

  private final PlayerReader playerReader;
  private final PathFileReader pathFileReader;
  private final GoalReader goalReader;

  /**
   * The constructor of the GameReader takes in all the other file readers use it to read certain
   * attributes and set it into the game object.
   */
  public GameReader() {
    playerReader = new PlayerReader();
    pathFileReader = new PathFileReader();
    goalReader = new GoalReader();
  }

  /**
   * A method for reading game from a filepath. It reads and takes in all the info and use it to
   * initiate a game.
   *
   * @param filePath the file path of the game
   * @return returns the game
   * @throws IOException if the filepath is invalid
   */
  public Game readGameFromFile(String filePath) throws IOException {
    Player player = readPlayerFromFile(filePath);
    Story story = readStoryFromFile(filePath);
    List<Goal> goals = readGoalsFromFile(filePath);
    return new Game(player, story, goals);
  }

  /**
   * a method for reading player from file. this method will read a temporary file that is created
   * from the createTempFile method. The temporary file is similar to the .player file, so we use
   * the playerReader to read the player and return the player,
   *
   * @param filepath the file path of the game file
   * @return returns the player
   * @throws IOException if the playerRead cannot find the temporary file.
   */
  private Player readPlayerFromFile(String filepath) throws IOException {
    String playerSection = readSection(filepath, "#Player");
    String tempFilePath = createTempFile(playerSection);
    return playerReader.readPlayerFile(tempFilePath);
  }

  /**
   * a method for reading story form file. This method will read a temporary file that is created
   * from the createTempFile method. The temporary file is similar to the .paths file, so we can use
   * pathFileReader to read the story and return the story.
   *
   * @param filepath the file path of the game
   * @return returns the story
   * @throws IOException if the pathFileReader cannot find the temporary file.
   */
  private Story readStoryFromFile(String filepath) throws IOException {
    String storySection = readSection(filepath, "#Story");
    String tempFilePath = createTempFile(storySection);
    return pathFileReader.readPathFile(tempFilePath);
  }

  /**
   * a method for reading goals from file. This method will read a temporary file that is created
   * from the createTempFile method. The temporary file is similar to the .goals file, so we can use
   * goalsReader to read the goals and return the  list of goals.
   *
   * @param filepath
   * @return returns the list of goals
   * @throws IOException
   */
  private List<Goal> readGoalsFromFile(String filepath) throws IOException {
    String goalsSection = readSection(filepath, "#Goals");
    String tempFilePath = createTempFile(goalsSection);
    return goalReader.readGoalFromFile(tempFilePath);
  }

  /**
   * a method for writing a section of a player/story/goals and returning the section based on the
   * file it reads. This method is used to create the content inside the temporary files, so it can
   * be used to read with the reader-classes.
   *
   * @param filePath the filepath of the .game file.
   * @param section  the section of the text file, we want to divide.
   * @return returns a String of the section.
   */
  private String readSection(String filePath, String section) {
    try (Scanner scanner = new Scanner(new File(filePath))) {
      StringBuilder sectionContent = new StringBuilder();
      boolean sectionFound = false;

      while (scanner.hasNextLine()) {
        String line = scanner.nextLine().trim();
        if (line.equals(section)) {
          sectionFound = true;
          break; // found the start of the section, exit the loop
        }
      }

      if (sectionFound) {
        while (scanner.hasNextLine()) {
          String line = scanner.nextLine().trim();
          if (line.startsWith("---")) {
            break; // reached the end of the section, exit the loop
          }
          sectionContent.append(line).append("\n");
        }
      }
      return sectionContent.toString();
    } catch (IOException e) {
      System.err.println("Error reading file: " + filePath);
      e.printStackTrace();
      return null;
    }
  }

  /**
   * a method to create a temporary file, from the section that the readSection creates. These
   * temporary files will be used to be read by other file-reader classes, to return info that will
   * be used to the game object.
   *
   * @param content the content that is returned by the readSection
   * @return returns the path of the temporary file
   * @throws IOException if the file could not be created.
   */
  private String createTempFile(String content) throws IOException {
    File tempFile = File.createTempFile("temp", ".test");
    try (FileWriter writer = new FileWriter(tempFile)) {
      writer.write(content);
    }
    return tempFile.getAbsolutePath();
  }
}

