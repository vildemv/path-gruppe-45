package edu.ntnu.idatt2001.group45.filehandling;

import edu.ntnu.idatt2001.group45.data.Game;
import edu.ntnu.idatt2001.group45.data.goals.Goal;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * the GameWriter class is a class that focuses on converting a game object into a text file.
 */
public class GameWriter {

  /**
   * this method is the writing a game object into a game file.
   *
   * @param game the game object that is going to be written into a save
   * @return returns the text file that is written.
   */
  public File saveGameToFile(Game game) {
    String directoryPath =
        "src" + File.separator + "main" + File.separator + "resources" + File.separator + "games";
    File directory = new File(directoryPath);
    if (!directory.exists()) {
      directory.mkdirs(); // Create the directory if it doesn't exist
    }
    String fileName =
        game.getPlayer().getName() + game.getStory().getTitle().replace(" ", "") + ".game";
    String filePath = directoryPath + File.separator + fileName;
    File gameFile = new File(filePath);
    try (FileWriter writer = new FileWriter(filePath)) {
      writePlayerToFile(writer, game);
      writeStoryToFile(writer, game);
      writeGoalsToFile(writer, game);
      System.out.println("Game saved to file: " + filePath);
    } catch (IOException e) {
      System.err.println("Failed to save game to file: " + filePath);
      e.printStackTrace();
    }
    return gameFile;
  }

  /**
   * method for writing the player-section of the file.
   *
   * @param writer the file-writer
   * @param game   the game that will be written
   * @throws IOException if the file cannot be written
   */
  private void writePlayerToFile(FileWriter writer, Game game) throws IOException {
    writer.write("#Player\n");
    writer.write(game.getPlayer() + "\n");
    writer.write("---\n");
  }

  /**
   * method for writing the story-section of the file. it uses the toString to write out the story
   * section, since the toString is similar to the pathFileWriter.
   *
   * @param writer the file-writer
   * @param game   the game that will be written
   * @throws IOException if the file cannot be written
   */
  private void writeStoryToFile(FileWriter writer, Game game) throws IOException {
    writer.write("#Story\n");
    writer.write(game.getStory().toString() + "\n");
    writer.write("---\n");
  }

  /**
   * method for writing the goals-section of the file For writing this section, it uses the method
   * from the GoalWriter to convert goal into a string.
   *
   * @param writer the file-writer
   * @param game   the game that will be written
   * @throws IOException if the file cannot be written
   */
  private void writeGoalsToFile(FileWriter writer, Game game) throws IOException {
    writer.write("#Goals\n");
    GoalWriter goalWriter = new GoalWriter();

    for (Goal goal : game.getGoals()) {
      String goalString = goalWriter.convertGoalToString(goal);
      writer.write(goalString + "\n");
    }
    writer.write("---\n");
  }
}
