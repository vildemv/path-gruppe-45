package edu.ntnu.idatt2001.group45.filehandling;

import edu.ntnu.idatt2001.group45.data.Story;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * PathWriter class is a class that is responsible for writing a story object into a file.
 */
public class PathFileWriter {

  /**
   * this method takes in a story and writes it into a .path
   *
   * @param story the story that is written to a file
   * @return returns the written file
   */
  public File writeStory(Story story) {
    String filePath = story.getTitle().replace(" ", "") + ".path";
    File file = new File(filePath);
    try (FileWriter writer = new FileWriter(filePath)) {
      writer.write(story.toString());
      System.out.println("Story written and saved to file: " + story.getTitle() + ".path");
    } catch (IOException e) {
      System.err.println("Failed to write story to file: " + story.getTitle() + ".path");
      e.printStackTrace();
    }

    return file;
  }
}
