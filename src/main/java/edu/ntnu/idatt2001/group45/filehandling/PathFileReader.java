package edu.ntnu.idatt2001.group45.filehandling;

import edu.ntnu.idatt2001.group45.data.Link;
import edu.ntnu.idatt2001.group45.data.Passage;
import edu.ntnu.idatt2001.group45.data.Story;
import edu.ntnu.idatt2001.group45.data.actions.Action;
import edu.ntnu.idatt2001.group45.data.actions.GoldAction;
import edu.ntnu.idatt2001.group45.data.actions.HealthAction;
import edu.ntnu.idatt2001.group45.data.actions.InventoryAction;
import edu.ntnu.idatt2001.group45.data.actions.ScoreAction;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The pathFileReader is a class is responsible to read a file and return a story
 */
public class PathFileReader {

  /**
   * A method to read a paths file and create a story within this text file.
   *
   * @param filePath the path of the file
   * @return returns a story
   */

  public Story readPathFile(String filePath) {
    try (Scanner sc = new Scanner(new File(filePath))) {
      Passage currentPassage = null;
      String passageTitle = "";
      StringBuilder passageContent = new StringBuilder();
      Story story = null;
      String storyTitle = "";
      boolean firstLine = true;
      String nextLine;

      while (sc.hasNextLine()) {
        String line = sc.nextLine().trim();

        if (firstLine
            && !line.isEmpty()) { //first line with text in the file will always be the title
          storyTitle = line;  //setting the story title
          firstLine = false;  //change boolean to false so this block doesn't get iterate twice
          continue;
        }

        if (line.startsWith("::")) {
          passageTitle = line.substring(2).trim(); //setting the passage title
          nextLine = sc.nextLine();
          while (!nextLine.isEmpty() && !nextLine.startsWith("[") && !nextLine.startsWith("::")) {
            passageContent.append(nextLine).append("\n");
            if (!sc.hasNextLine()) {
              break;
            }
            nextLine = sc.nextLine();
          }
          currentPassage = getPassage(passageTitle, passageContent.toString());
          line = nextLine;
        }

        if (line.startsWith("[")) {
          while (line.startsWith("[")) {
            Link link = getLink(line);
            if (line.endsWith("}")) {
              List<Action> actions = readActions(line);
              for (Action action : actions) {
                link.addAction(action);
              }
            }
            assert currentPassage != null;
            currentPassage.addLink(link);
            if (!sc.hasNextLine()) {
              break;
            }
            line = sc.nextLine();
          }

        }
        //what happens if there are no links

        if (currentPassage != null) {
          if (story == null) {
            story = new Story(storyTitle,
                currentPassage); //if story hasn't been initialized yet, we can add
            // story title and the currentPassage which will always be the first passage
          }
          story.addPassage(currentPassage); //adding the passage to the story
          passageContent.delete(0, passageContent.length()); //reset values
          currentPassage = null; //reset values
        }
      }
      return story;
    } catch (IOException ex) {
      System.err.println("Error file not readable: " + filePath);
      ex.printStackTrace();
      return null;
    }

  }

  private Link getLink(String line) {
    String linkText = line.substring(line.indexOf("[") + 1, line.indexOf("]"));
    String linkRef = line.substring(line.indexOf("(") + 1, line.indexOf(")"));
    return new Link(linkText, linkRef); //creates link
  }

  private Passage getPassage(String title, String content) {
    return new Passage(title, content);
  }

  private List<Action> readActions(String line) {
    String actionText = line.substring(line.indexOf("{"), line.indexOf("}") + 1);
    String patternString = "(\\w+)=(\\w+|-\\d+)";
    Pattern pattern = Pattern.compile(patternString);
    Matcher matcher = pattern.matcher(actionText);

    List<Action> actions = new ArrayList<>();

    while (matcher.find()) {
      String actionType = matcher.group(1);
      String actionValue = matcher.group(2);

      Action action = switch (actionType) {
        case "gold" -> new GoldAction(Integer.parseInt(actionValue));
        case "score" -> new ScoreAction(Integer.parseInt(actionValue));
        case "health" -> new HealthAction(Integer.parseInt(actionValue));
        case "inventory" -> new InventoryAction(actionValue);
        default -> throw new IllegalArgumentException("Invalid action type: " + actionType);
      };

      actions.add(action);
    }

    return actions;
  }
}