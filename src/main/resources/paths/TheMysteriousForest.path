The mysterious forest

::The dark night
You find yourself standing at the edge of a dense forest.
The air is filled with an eerie silence, and a faint mist blankets the ground.
The path ahead is obscured by towering trees and tangled vegetation. You break of a stick
and prepare to use it as a weapon.
[Follow a narrow trail](Into the Unknown){score=50,inventory=stick}
[Turn back and leave](End of Adventure){score=-1000,gold=-1000,health=-1000}

::Into the Unknown
You decide to follow a narrow trail that winds deeper into the forest.
As you venture forth, the shadows grow thicker,
and the sounds of rustling leaves fill the air. You can't shake off
the feeling that you're being watched.
[Investigate the rustling sound](Hidden Surprise){health=-10}
[Press on, undeterred](Lost in the Woods){health=-20}

::Hidden Surprise
Curiosity gets the better of you, and you cautiously approach the source of the rustling sound.
To your amazement, you discover a family of playful squirrels scurrying about.
They seem unbothered by your presence, and you feel a
momentary sense of peace amidst the mysterious forest.
[Continue deeper into the forest](Enchanted Glade){score=30}
[Return to the main trail](Into the Unknown)

::Lost in the Woods
You forge ahead, determined to navigate the labyrinthine forest.
However, the dense foliage and twisted paths confound your senses.
Hours turn into days, and days into weeks as you remain hopelessly lost.
The forest consumes you, and your chances of escape grow slim.
[Accept your fate](Trapped Forever){health=-1000}
[Keep searching for a way out](Glimmer of Hope){health=-30}

::Trapped Forever
Accepting your fate, you resign yourself to the
inescapable clutches of the enchanted forest.
Time fades away as you transform into a statue,
forever guarding its secrets.

::Enchanted Glade
As you journey deeper into the forest, a soft glow beckons you from afar.
You follow the ethereal light, and it leads you
to an enchanting glade. Sunlight filters through the canopy above,
casting a warm glow on the vibrant flora below. You feel a renewed sense of hope and purpose.
[Rest and rejuvenate](Recharged){health=20,inventory=sunflower}
[Explore the glade further](Unveiling Secrets){score=40}


::Recharged
You take a moment to rest and replenish your energy in the tranquil glade.
The soothing ambiance and harmonious melodies of nature revive your spirit. With newfound strength,
you continue your journey through the mysterious forest.
[Resume your exploration](Unveiling Secrets)

::Unveiling Secrets
As you delve deeper into the glade, you stumble upon a hidden path veiled by ancient trees.
Intrigued, you cautiously follow the path, guided by an unseen force. Soon, you encounter a
mystical entity, the Guardian of Secrets.
[Seek the wisdom of the guardian](Divine Knowledge){score=100}
[Challenge the guardian](Battle of Wits){score=-50}

::Divine Knowledge
The guardian imparts profound wisdom and ancient knowledge, illuminating the mysteries of the
forest and beyond. Your mind expands with newfound understanding, and you become a vessel of
wisdom, destined to share your enlightenment with the world.

::Battle of Wits
You engage in a fierce mental duel with the guardian, testing your intellect and cunning.
After a grueling contest of wits, you emerge victorious, having outsmarted the ancient entity.
Your triumph echoes through the forest, and the path ahead opens wide.
[Step into the unknown](The Final Frontier){score=80}

::The Final Frontier
You step forward, embracing the unknown with courage and curiosity. The forest
reveals its final secret—a portal that transports you to realms yet unexplored.
With each step, you embark on a new adventure, forever changed by the enigmatic journey
through the mysterious forest.

::End of Adventure
Overwhelmed by the unknown and uncertain dangers that lie ahead, you turn back and
retreat from the mysterious forest. Though your path ends here, the memories of this
peculiar encounter will linger, forever a reminder of the untrodden paths and uncharted
territories that await the bold. You will always be known as a coward.